var fs = require('fs'),
	path = require('path'),
	crypto = require('crypto');
Core.define({
	methodsDir: "methods",
	dbConnector: null,
	ctor: function() {
		var me = this;
		me.dbConnector = Core.Database.getCurrentConnector();
	},
	loadMethods: function(dirname) {
		var dir = path.normalize(dirname + '/' + this.methodsDir),
			files;
		if (fs.existsSync(dirname) && (files = fs.readdirSync(dir))) {
			for (var i in files) {
				if (files[i].indexOf('.js') < 0)
					continue;
				var fpath = path.normalize(dir + '/' + files[i]),
					methodName = files[i].replace('.js', '');
				Logger.debug('load method', methodName, fpath);
				try {
					var methods = require(fpath);
					if (typeof methods == 'function')
						this[methodName] = methods;
					else
					{
						for(var _methodName in methods)
						{
							this[_methodName] = methods[_methodName];
						}
					}
				} catch (e) {
					Logger.error('fail to load method', methodName,'error :',e);
				}
			}
		}
	},

});