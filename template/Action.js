var qs = require('querystring')

Core.define({

    ctor: function() {},
    prepare: function(req, resp) {
        var me = this;

        me.request = req;
        me.response = resp;
        if (me.VALID_GET) {
            try {
                me.validParams(req._GET, me.VALID_GET);
            } catch (e) {
                me.fireEvent('error', e);
                return;
            }
        }
        if (req.method == 'POST') {
            var body = [];
            req.on('data', function(data) {
                body.push(data);
                // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
                if (body.length > 1e6) {
                    // FLOOD ATTACK OR FAULTY CLIENT, NUKE REQUEST
                    req.connection.destroy();
                }
            });
            req.on('end', function() {
                req._POST = qs.parse(body.join(''));
                // use POST
                try {
                    me.proceed(req, resp, function(data, options, respcbk) {
                        if (me.method && !me.failed) {
                            me.service[me.method](data, options, respcbk ||   function(err, resp) {
                                me.writeData(err ? null : resp, err);
                            });
                        }
                    });
                    me.fireEvent('postend', null, req, resp);
                } catch (e) {
                    me.fireEvent('error', e);
                }
            });
        } else {
            me.proceed(req, resp, function(data, options, respcbk) {
                if (me.method && !me.failed) {
                    me.service[me.method](data, options, respcbk ||   function(err, resp) {
                        me.writeData(err ? null : resp, err);
                    });
                }
            });
        }
    },
    proceed: function(req, resp) { /*virtual methods*/ },
    success: function(success, data) {
        var resp = {
            success: success && true || false,
        };
        if (data)
            resp.data = data;
        this.response.end(JSON.stringify(resp));
    },
    error: function(reason) {
        this.response.end(['{"success":false,"error":"', reason, '"}'].join(''));
    },
    notfound: function() {
        this.response.writeHead(404, {});
        this.response.end();
    },
    notImplemented: function() {
        this.response.writeHead(501, {});
        this.response.end();
    }
});