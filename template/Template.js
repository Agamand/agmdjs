var async = Core.Async,
	fs = require('fs');
Core.define({
	requires: ['AgmdJS.template.Fragment'],
	extendOf: "AgmdJS.template.Action",
	fragments: {

	},
	ctor: function() {
		StaticHtmlTemplate.register(this, this.alias);
	},
	view: function(ctx, buffer, callback) {
		callback();
	},

	model: function(ctx, buffer, callback) {
		callback();
	},
	proceed: function(req, resp) {
		this.draw(StaticHtmlTemplate.createContext(req, resp), resp);
	},
	draw: function(ctx, buffer) {
		var me = this;
		StaticHtmlTemplate.compute(this, ctx, buffer, function() {
			me.debug('end of draw', me.className);
			buffer.end();
		});
	},
	loadFileScript: function(path, callback) {
		fs.readFile(path, callback);
	}
});

//static parts
/*
var Header = Core.define('AgmdJS.template.Header', {
	extendOf: 'AgmdJS.template.Template',
	alias: 'main_header',
	view: function(ctx, buffer, callback) {
		buffer.write([
			'<html>',
			'<head>',
			'<meta name="Content-Language" content="fr">',
			'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">',
			'<script type="text/javascript" src="/res/agmdjs/js/jquery.min.js"></script>',
			'<script type="text/javascript" src="/res/agmdjs/js/jquery.tablesorter.min.js"></script>',
			'<script type="text/javascript" src="/res/agmdjs/js/common/Core.js"></script>',
			'<script type="text/javascript" src="/res/agmdjs/js/bootstrap.min.js"></script>',
			'<link rel="stylesheet" type="text/css" href="/res/agmdjs/css/style.css">',
			'<link rel="stylesheet" type="text/css" href="/res/agmdjs/css/bootstrap.min.css">'
		].join(''));
		if (ctx.scripts) {
			for (var i in ctx.scripts) {
				buffer.write(['<script type="text/javascript" src="', ctx.scripts[i], '"></script>'].join(''));
			}
		}
		if (ctx.styles) {
			buffer.write(['<style type="text/css">', ctx.styles.join(''), '</style>'].join(''));
		}

		buffer.write('</head><body>');
		callback();
	}
});
var Footer = Core.define('AgmdJS.template.Footer', {
	extendOf: 'AgmdJS.template.Template',
	alias: 'main_footer',
	view: function(ctx, buffer, callback) {
		buffer.write('</body></html>');
		callback();
	}
});
*/