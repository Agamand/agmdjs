var mime = require('mime-types'),
	path = require('path'),
	fs = require('fs'),
	async = require('async'),
	cacheDuration = Core.getConfig('resources.cacheDuration') || -1,
	Packager = Core.include('AgmdJS.packager.Packager');
Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var file = req._GET.file,
			me = this;

		me.getResourcesPath(file, function(err, filePath) {
			if (err) {
				me.fireEvent('error', err);
				return;
			}
			if (!filePath) {
				me.fireEvent('notfound');
				return;
			}
			var headers = {
					//'Content-Length': body.length,
					"Content-Type": mime.contentType(path.extname(filePath))
				},
				today = new Date(),
				client_last_modified = req.headers['if-modified-since'] && new Date(req.headers['if-modified-since']) || null;
			Packager.getPackagedFile(filePath, function(err, filePath) {

				fs.stat(filePath, function(err, stat) {
					Logger.debug(client_last_modified,stat.mtime,(stat.mtime - client_last_modified));
					if (client_last_modified && (stat.mtime - client_last_modified) < 1000) {
						Core.apply(headers, {
							'Cache-Control': 'public, must-revalidate, proxy-revalidate',
							//'Expires': new Date(+stat.mtime + cacheDuration),
							'Last-Modified': stat.mtime
						});
						me.response.writeHead(304, headers);
						me.response.end();
					} else {

						Core.apply(headers, {
							'Cache-Control': 'public, must-revalidate, proxy-revalidate',
							'Expires': new Date(+today + cacheDuration),
							'Last-Modified': stat.mtime
						});
						resp.writeHead(200, headers);
						fs.createReadStream(filePath).pipe(resp);
					}
				});

			})

		});
	},
	getResourcesPath: function(file, callback) {
		var me = this,
			namespace = Core.getConfig("namespace"),
			filePath = null,
			tasks = [];

		/*for (var i in namespace) {
			(function(nsPath) {

				tasks.push(function(callback) {
					var fpath = path.normalize(nsPath + file);
					Logger.debug('test path',fpath);
					fs.exists(fpath, function(exists) {
						if (exists)
							filePath = fpath;
						callback();
					});
				});
			})(namespace[i]);
		}
		async.parallel(tasks, function(err) {
			callback(err, filePath);
		})*/


		var parts = file.split('/');
		me.debug(parts, namespace, file);
		if (namespace[parts[0]]) {
			parts[0] = namespace[parts[0]];
			var filename = parts.join('/');
			fs.exists(filename, function(exists) {
				callback(null, exists && filename || null);
			});
		} else callback();


	}
});