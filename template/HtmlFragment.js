var Mustache = require("mustache");

Core.define({
	extendOf: "AgmdJS.template.Fragment",
	filename: null, //path of template file
	ctor: function() {
		var me = this;
		me._super();
		if (!me.filename) {
			me.filename = me.namespace.split('.');
			me.filename.push(me.filename[me.filename.length - 1]);
			me.filename[me.filename.length - 2] = 'views';
			me.filename = me.filename.join('.');
		}
		me.loadFile();
	},
	loaded: false,
	view: function(ctx, buffer, callback) {
		var me = this;
		me.onLoaded(function() {
			var output = Mustache.render(me.template, ctx.view || {});
			buffer.write(output);
			callback();
		});
	},
	onLoaded: function(callback) {
		var me = this;
		if (!me.loaded) {
			me.once('load', callback);
		} else callback();
	},
	loadFile: function() {
		var me = this;
		Logger.debug('loadFile', me.filename);
		Core.loadFileFromPathOrNamespace(me.filename, '.html', function(err, data) {
			if (!err && data) {
				me.template = data;
				me.loaded = true;
				me.fireEvent('load');
			} else Logger.error('fail load template', me.filename, ' err : ', err || 'no error');
		});
	}
});