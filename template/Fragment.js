var async = Core.Async,
	fs = require('fs'),
	HtmlBuilder = Core.include('AgmdJS.tools.HtmlBuilder');
Core.define({
	ctor: function() {
		StaticHtmlTemplate.register(this, this.alias);
	},
	view: function(ctx, buffer, callback) {
		callback();
	},
	model: function(ctx, callback) {
		callback();
	},
	draw: function(ctx, buffer, callback) {
		var me = this;
		me.model(ctx, function() {
			me.view(ctx, buffer, callback);
		})
	},

	loadFileScript: function(path, callback) {
		fs.readFile(path, callback);
	}
});
StaticHtmlTemplate = {};

StaticHtmlTemplate.createContext = function(req, resp) {
	var ctx = {
		req: req,
		resp: resp,
		view: {},
		addScript: function(script, after) {
			ctx["scripts" + (after ? "After" : "")] = ctx["scripts" + (after ? "After" : "")] || [];
			ctx["scripts" + (after ? "After" : "")].push(script);
		},
		addStyles: function(style, after) {
			ctx["styles" + (after ? "After" : "")] = ctx["styles" + (after ? "After" : "")] || [];
			ctx["styles" + (after ? "After" : "")].push(style);
		},
		addInlinesScripts: function(script, after) {
			Logger.debug('addInlinesScript', script);
			script = script instanceof Array ? script : [script];
			ctx["inlinesScripts" + (after ? "After" : "")] = ctx["inlinesScripts" + (after ? "After" : "")] || [];
			for (var i in script) {
				ctx["inlinesScripts" + (after ? "After" : "")].push(script[i]);
			}
		},
		addInlinesStyles: function(styles, after) {
			styles = styles instanceof Array ? styles : [styles];
			ctx["inlinesStyles" + (after ? "After" : "")] = ctx["inlinesStyles" + (after ? "After" : "")] || [];
			for (var i in styles) {
				ctx["inlinesStyles" + (after ? "After" : "")].push(styles[i]);
			}
		}
	};
	return ctx;

}
StaticHtmlTemplate.compute = function(obj, ctx, res, callback) {
	var buffer = res;
	StaticHtmlTemplate.computeModel(obj, ctx, res, function() {
		buffer.write([
			'<html>',
			'<head>',
			'<meta name="Content-Language" content="fr">',
			'<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">',
			'<script type="text/javascript" src="/res/agmdjs/js/jquery.min.js"></script>',
			'<script type="text/javascript" src="/res/agmdjs/js/zlib.js"></script>',
			'<script type="text/javascript" src="/res/agmdjs/js/common/Core.js"></script>',
			'<link rel="stylesheet" type="text/css" href="/res/agmdjs/css/style.css">'
		].join(''));



		if (ctx.scripts) {
			for (var i in ctx.scripts) {
				buffer.write(['<script type="text/javascript" src="', ctx.scripts[i], '"></script>'].join(''));
			}
		}

		if (ctx.styles) {
			for (var i in ctx.styles) {
				buffer.write(['<link rel="stylesheet" type="text/css" href="', ctx.styles[i], '">'].join(''));
			}
		}

		Logger.debug('inlinesScripts', ctx.inlinesScripts);
		if (ctx.inlinesScripts) {
			for (var i in ctx.inlinesScripts) {
				buffer.write(['<script>', ctx.inlinesScripts[i], '</script>'].join(''));
			}
		}

		if (ctx.inlinesStyles) {
			buffer.write(['<style type="text/css">', ctx.inlinesStyles.join(''), '</style>'].join(''));
		}

		buffer.write('</head>');

		var attrs = {};
		var body = HtmlBuilder.body({
			attrs: attrs
		}, buffer);
		ctx.body = body;

		StaticHtmlTemplate.computeView(obj, ctx, res, function() {
			if (ctx.scriptsAfter) {
				for (var i in ctx.scriptsAfter) {
					buffer.write(['<script type="text/javascript" src="', ctx.scriptsAfter[i], '"></script>'].join(''));
				}
			}
			if (ctx.stylesAfter) {
				for (var i in ctx.stylesAfter) {
					buffer.write(['<link rel="stylesheet" type="text/css" href="', ctx.stylesAfter, '">'].join(''));
				}
			}
			if (ctx.inlinesScriptsAfter) {
				for (var i in ctx.inlinesScriptsAfter) {
					buffer.write(['<script>', ctx.inlinesScriptsAfter[i], '</script>'].join(''));
				}
			}
			if (ctx.inlinesStylesAfter) {
				buffer.write(['<style type="text/css">', ctx.inlinesStylesAfter.join(''), '</style>'].join(''));
			}
			body.close();
			buffer.write('</html>');
			callback();
		});
	});
}

StaticHtmlTemplate.computeModel = function(obj, ctx, res, callback) {
	var frags = obj.fragments;
	var asyncTask = [];
	for (frag in frags) {
		if (StaticHtmlTemplate.fragments[frag]) {
			asyncTask.push((function(_frag) {
				return function(callback) {
					Logger.debug('compute model', _frag);
					//StaticHtmlTemplate.computeModel(StaticHtmlTemplate.fragments[_frag], ctx, res, callback)
					StaticHtmlTemplate.fragments[_frag].model(ctx, callback, callback);
				};
			})(frag));
		}
	}

	Logger.debug('model to compute', asyncTask.length)

	async.series(asyncTask, callback);
}

StaticHtmlTemplate.computeView = function(obj, ctx, res, callback) {
	var frags = obj.fragments;
	var asyncTask = [];
	for (frag in frags) {

		Logger.debug('search frag', frag, !!StaticHtmlTemplate.fragments[frag]);
		if (StaticHtmlTemplate.fragments[frag]) {
			asyncTask.push((function(_frag) {
				return function(callback) {
					Logger.debug('compute view', _frag);
					//StaticHtmlTemplate.computeView(StaticHtmlTemplate.fragments[_frag], ctx, res, callback)
					StaticHtmlTemplate.fragments[_frag].view(ctx, res, callback);
				};
			})(frag));
		}
	}
	async.series(asyncTask, callback);
}
StaticHtmlTemplate.fragments = {};
StaticHtmlTemplate.register = function(me, alias) {

	Logger.debug('fragment register', me.className, me.alias);

	StaticHtmlTemplate.fragments[me.className] = me;
	if (alias)
		StaticHtmlTemplate.fragments[alias] = me;
}