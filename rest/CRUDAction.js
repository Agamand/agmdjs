Core.define({
	extendOf: "AgmdJS.template.Action",
	_path: '/',
	_accept: {
		'application/json': 1
	},
	_resultType: {
		'application/json': 1
	},
	type: {
		POST: 'insert',
		GET: 'get',
		PUT: 'update',
		PATCH: 'update',
		DELETE: 'delete'
	},
	ctor: function() {

	},
	proceed: function(req, resp) {
		if (this.type[req.method]) {
			this[this.type[req.method]](req, resp);
		} else {
			me.error(['unsupported method : ', req.method])
		}
	},

	insert: function() {
		this.notImplemented();
	},
	get: function() {
		this.notImplemented();
	},
	update: function() {
		this.notImplemented();
	},
	delete: function() {
		this.notImplemented();
	},
	success: function(data) {
		if (!data) {
			this.notfound();
			return;
		}
		//TODO set contenttype based on request accept-type and possible _resultType
		this.response.writeHead(200, {
			'Content-Type': 'application/json'
		});
		this.response.end(JSON.stringify(data));
	},
	error: function(reason) {
		this.response.writeHead(500, {
			"reason": reason
		});
		this.response.end();
	},


});