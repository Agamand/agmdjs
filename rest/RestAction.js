Core.define({
	extendOf: "AgmdJS.Rest.CRUDAction",
	_path: '/',
	_accept: {
		'application/json': 1
	},
	_resultType: {
		'application/json': 1
	},
	ctor: function() {

	},
	proceed: function(req, resp) {
		
		//TODO check income type based on _accept
		this._super(req,resp);
	},
	success: function(data) {
		if (!data) {
			this.notfound();
			return;
		}
		//TODO set contenttype based on request accept-type and possible _resultType
		this.response.writeHead(200, {
			'Content-Type': 'application/json'
		});
		this.response.end(JSON.stringify(data));
	},
	error: function(reason) {
		this.response.writeHead(500, {
			"reason": reason
		});
		this.response.end();
	},
	notfound: function() {
		this.response.writeHead(404, {});
		this.response.end();
	}
});