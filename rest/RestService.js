var fs = require('fs'),
    path = require('path');
Agmd.define({
    extendOf: 'AgmdJS.service.Service',
    action: {},
    baseRestPath: '/rest/1.0',
    restRoutingTree: {},
    restDir: "rest",
    ctor: function(cfg) {
        var me = this;
        me._super(cfg);
    },
    loadRest: function(dirname) {
        var me = this,
            dir = path.normalize(dirname + '/' + this.restDir),
            files;
        if (fs.existsSync(dirname) && (files = fs.readdirSync(dir))) {
            for (var i in files) {
                if (files[i].indexOf('.js') < 0)
                    continue;
                var fpath = path.normalize(dir + '/' + files[i]),
                    actionName = files[i].replace('.js', '');
                me.debug('load method', actionName, fpath);
                try {
                    var actionClazz = require(fpath);
                    var action = this.action[actionName] = new actionClazz();

                    me.debug("action", typeof action, typeof actionClazz);
                    action.service = me;
                    var paths = action._path;
                    if (!(paths instanceof Array)) {
                        paths = [paths];
                    }
                    for (var j in paths) {
                        var _path = paths[j];
                        var url,
                            args = null;
                        if (typeof _path == "object") {
                            url = _path.url;
                            args = _path.args;
                        } else
                            url = _path.url;
                        Agmd.Route._addRoute(me.restRoutingTree, url, Agmd.Route.restAction(action, args || {}));
                    }
                } catch (e) {
                    me.error('fail to load method', actionName, e, e.stack);
                }
            }
        }
    },
    proceed: function(query, req, resp) {
        var me = this,
            restQuery = query.substring(me.baseRestPath.length);
        var routeResult = Agmd.Route.processPath(restQuery, me.restRoutingTree);
        if (routeResult && routeResult.rest) {
            req._REST = routeResult.args || {};
            routeResult.rest.prepare(req, resp);
        } else {
            throw new Error(['unhandle rest path :', restQuery].join(' '));
        }
    }

});