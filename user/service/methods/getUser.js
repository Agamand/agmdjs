var async = Core.Async,
	getUser = function(data, opt, callback) {

		var me = this;

		me.dbConnector.findOne(me.database, me.collection.users, {
			filter: data,
			project: opt && opt.project || {}
		}, function(err, data) {

			if (!data || err) {
				cb(err);
				return;
			}
			callback(null, data);
		});



	};
module.exports = getUser;