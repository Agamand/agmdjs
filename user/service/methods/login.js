var login = function(data, opt, callback) {
	var me = this;
	if (!data.login || !data.password) {
		callback("fail");
		return;
	}
	var passwordHash = me.generatePasswordHash(data.password);
	me.dbConnector.findOne(me.database, me.collection.users, {
			filter: {
				login: data.login,
				password: passwordHash
			},
			project: {
				_id: 1
			}
		},
		function(err, data) {
			if (!data || err) {
				callback(err);
				return;
			}
			callback(null, data);
		});
};
module.exports = login;