var async = Core.Async,
	createUser = function(data, opt, callback) {

		var me = this;

		if (!data.login || !data.password) {
			callback('fail');
			return;
		}

		var passwordHash = me.generatePasswordHash(data.password);
		var result;
		async.series([function(cb) {
			me.dbConnector.findOne(me.database, me.collection.users, {
				filter: {
					login: data.login
				}
			}, function(err, data) {

				if (!data || err) {
					cb(err);
					return;
				}
				cb(['user', data.login, 'already exists'].join(' '));
			});

		}, function(cb) {
			me.dbConnector.insert(me.database, me.collection.users, data, function(err, data) {

				if (err) {
					cb(err);
					return;
				}
				result = true;
				cb();
			});
		}], function(err) {
			callback(err, result);
		});



	};
module.exports = createUser;