var crypto = require('crypto');
Core.define({
	extendOf: 'AgmdJS.service.Service',
	database: 'user',
	passwordSalt: "AgmdJSSALT",
	collection: {
		users: 'users'
	},
	ctor: function(cfg) {
		var me = this;
		me._super(cfg);
		me.loadMethods(__dirname);
	},
	generatePasswordHash: function(password) {
		var me = this;
		return crypto.createHash('sha1', me.passwordSalt).update(password).digest('hex');
	}

});