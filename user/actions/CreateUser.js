Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var me = this,
			login, password;
		try {
			login = req._GET.login;
			password = req._GET.password;
			if (!login || !password)
				throw new Error('fail args');
		} catch (e) {
			me.error(e);
			return;
		}

		var userService = Core.Service.getInstance('AgmdJS.user.service.UserService');
		userService.createUser({
			login: login,
			password: password
		}, null, function(err, result) {
			if (err) {
				me.error(err);
			} else me.success(result);
		});
	}
});