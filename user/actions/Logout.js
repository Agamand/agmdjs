Core.define({
	extendOf: "AgmdJS.template.Action",
	proceed: function(req, resp) {
		var me = this,
			userService = Core.Service.getInstance('AgmdJS.user.service.UserService');
		if (req._USER) {
			req._SERVER.removeSessionCookie(req, resp, function(err) {
				me.success(!err);
			});
		} else me.success(false);
	}
});