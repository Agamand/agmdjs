require('../index.js');



Logger.info('ProgressBar', typeof Core.ProgressBar.once);


Core.Async.series([function(cb) {
	console.log('start test 1');
	Core.ProgressBar.start(100000);

	var update = setInterval(function() {
		Core.ProgressBar.update(400);
		//console.log('tick1')
	}, 10)

	Core.ProgressBar.once('complete', function() {
		clearInterval(update);
		console.log('end test 1');
		cb();
	});
}, function(cb) {
	console.log('start test 2');
	Core.ProgressBar.start(100000, 'test2');

	var update2 = setInterval(function() {
		Core.ProgressBar.update(100);
	}, 10)

	Core.ProgressBar.once('complete', function() {
		sdf();
		clearInterval(update2);
		console.log('end test 2');
		cb();

	});
}], function() {
	Logger.info('test ending')
})