require('../index.js');


Core.define('toto.one', {
	toto: function() {
		console.log("toto1")
	}
});
Core.define('toto.two', {
	extendOf: 'toto.one',
	ctor: function() {
		console.log(this._super);
	},
	toto: function() {
		this._super();
		console.log('toto2');
	}
});

Core.include('AgmdJS.template.HTm')

var titi = (new toto.two())
titi.toto();
console.log('titi = ', titi);