var crypto = require('crypto'),
    zlib = require('zlib'),
    md5 = crypto.createHash('md5');
md5.update('1(Dp9ç1&@!.F/DF?.T<)OJNFK2');
var key = md5.digest('binary'),
    md5 = crypto.createHash('md5');
md5.update(key);
var iv = new Buffer('0123456789012345', 'ascii');

var Decode = function(value, callback) {
    var decipher = crypto.createDecipheriv('aes-128-cbc', key, iv);

    var result = [];
    try {
        result.push(decipher.update(value, 'base64', 'binary'));
        result.push(decipher.final('binary'));
    } catch (e) {
        callback('fail to decipher '+e.message);
        return;
    }

    zlib.inflate(new Buffer(result.join(''), 'binary'), function(err, def) {
        try {
            callback(null, JSON.parse(def));
        } catch (e) {
            callback('fail to inflate');
        }
    });


}

var Encode = function(value, callback) {

    zlib.deflate(new Buffer(JSON.stringify(value), 'utf8'),
        function(err, data) {
            console.log(typeof data, data instanceof Buffer);
            console.log(data);

            var cipher = crypto.createCipheriv('aes-128-cbc', key, iv);

            var result = [];
            try {
                result.push(cipher.update(data, 'binary', 'base64'));
                result.push(cipher.final('base64'));
                callback(null, result.join(''))
            } catch (e) {
                callback('fail to cipher '+e.message);
                return;
            }

        });
}

Encode({
    _id: 1,
    login: "toto"
}, function(err, value) {
    console.log('err',err)
    Decode(value, function(err, value) {
        console.log('err',err)
        console.log('Encode and decode value', value);
    })
});