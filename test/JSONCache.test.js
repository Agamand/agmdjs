require('../index.js');
var path = require('path');
Core.config({
	cache: {
		cacheDIR: path.normalize(__dirname + '/../../testcache/'),
		defaultCacheDuration: 5
	}
})


var JSONCache = Core.include('AgmdJS.cache.JSONCache');

var data = {
	toto: 1,
	tutu: "2"
};
JSONCache.getCache("testjson", function(err, cache) {
	Logger.debug('cacheStatus', cache);
	if (cache.cacheStatus != JSONCache.cacheStatus.GOOD) {
		Logger.error('no cache or oudated', err);
		JSONCache.saveCache("testjson", data, {}, function() {
			process.exit(0);
		})
	} else {
		Logger.debug('first get cache', cache.data);
		JSONCache.getCache("testjson", function(err, cache) {
			Logger.debug('retry with memory cache', cache.data);
			process.exit(0);
		});
	}

});