require('../index.js');
var path = require('path');
Core.config({
	cache: {
		cacheDIR: path.normalize(__dirname + '/../../testcache/'),
		defaultCacheDuration: 5
	}
})


var FileCache = Core.include('AgmdJS.cache.FileCache');

var data = "Do you see any Teletubbies in here?";
FileCache.getCache("test", function(err, cache) {
	Logger.debug('cacheStatus', cache.cacheStatus);
	if (cache.cacheStatus != FileCache.cacheStatus.GOOD) {
		Logger.error('no cache or oudated', err);
		FileCache.saveCache("test", data, {}, function() {
			process.exit(0);
		})
	} else {
		Logger.debug('first get cache', cache.data.toString());
		process.exit(0);
	}

});