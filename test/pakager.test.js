require('../index.js');
var path = require('path');
Core.config({
	packager: {
		enable: true,
		runtime: true, // resources are package on start server
		onDemand: true, // resources are package on resources query
		packageDir: path.normalize(__dirname + "/../../packaged_res/")
	},
})


var Packager = Core.include('AgmdJS.packager.Packager');

var jsFile = path.normalize(__dirname + '/../res/js/common/Core.js'),
	cssFile = path.normalize(__dirname + '/../res/css/market.css');
Packager.getPackagedFile(jsFile, function(err, data) {

	Logger.debug(err, data);
})
Packager.getPackagedFile(cssFile, function(err, data) {

	Logger.debug(err, data);
})