require('../index.js');
var dummies = [{
	id: 1,
	name: 'dummy1',
	data: {
		title: "t1"
	}
}, {
	id: 2,
	name: 'dummy2',
	data: {
		title: "t2"
	}
}, {
	id: 3,
	name: 'dummy3',
	data: {
		title: "t3"
	}
}];



var filter = {
	id: 1,
	blabla: {
		$in: [1, 2, 3]
	},
	machin: {
		truc: {
			chose: 5
		}
	}
}
Logger.info(filter,Core.getFullyQualifiedObject(filter));