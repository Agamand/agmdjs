var path = require('path');
Core.configure({
	/*namespace: {
		'NAMESPACE': path.normalize(PATH)
	}*/
	port: 9000,
	/*	pathPrefix: 'NAMESPACE',*/
	packager: {
		enable: true, // use package resources
		runtime: false, // resources are packaged at start
		onDemand: false, // resources are package on query
		packageDir: path.normalize(__dirname + "../../packaged_res/")
	},
	routing: [
		//['//test//hello.html', url('/hello.html')],
	],
	http: {
		gzip: true,
		ssl: false
	},
	cache: {
		cacheDIR: path.normalize(__dirname + '/../../webcache/'),
		defaultCacheDuration: 3600
	},
	angularJS: {
		application: 'myApp'
	},
	database: {
		mongodb: {
			host: '127.0.0.1'
		}
	},
	resources: {
		cacheDuration: 3600 * 24 * 30 * 1000 // -1 infinity cache, 0 to infinty ms 
	},
	inline:{
		
	}

});