var CleanCSS = require('clean-css'),
    cleanCSS = new CleanCSS(),
    fs = require('fs'),
    path = require('path'),
    async = Agmd.Async,
    UglifyJS = require("uglify-js"),
    packageDir = Agmd.getConfig('packager.packageDir') || '../../packaged_res/',
    packagerCfg = Agmd.getConfig('packager', {});
Logger.debug('packageDir', packageDir)
Agmd.define({
    singleton: true,
    ALLOWED_TYPE: {
        'css': 1,
        'js': 1
    },
    __waitCall: {

    },
    __inProcess: {

    },
    ctor: function() {
        Agmd.Fs.makePathSync(packageDir + 't');
    },
    addToWaitCall: function(type, callback) {
        var me = this;
        me.__waitCall[type] = me.__waitCall[type] || [];
        me.__waitCall[type].push({
            callback: callback
        });
    },
    processWaitCall: function(type, err, result) {
        var me = this;
        if (!(me.__waitCall[type]))
            return;
        var waitCall = me.__waitCall[type];

        delete me.__waitCall[type];
        for (var i in me.__waitCall[type]) {
            var elt = me.__waitCall[type][i];
            elt.callback(err, result);
        }

    },
    packageDirectory: function(dirPath, callback) {},

    getPackagedFile: function(filePath, callback) {

        var me = this,
            ext = path.extname(filePath).replace(/\./, '');
        if (!packagerCfg.enable) {
            callback(null, filePath);
            return;
        }
        if (!(ext in me.ALLOWED_TYPE)) {
            me.debug('ext', ext, 'is not packageble');
            callback(null, filePath);
            return;
        }
        if (me.__inProcess[filePath]) {
            me.addToWaitCall(filePath, callback);
            return;
        }
        me.__inProcess[filePath] = true;
        me.packager[ext](filePath, false, function(err, result) {
            if (err) {
                Logger.error(err);
            }
            callback(err, result);
            delete me.__inProcess[filePath];
            me.processWaitCall(filePath, err, result);
        });

    },



    packager: {
        checkFileStat: function(orig, pack, callback) {
            var me = this;
            var statOrig,
                statPack;
            async.series([function(cb) {
                fs.stat(orig, function(err, stat) {
                    statOrig = stat;
                    cb();
                });
            }, function(cb) {
                fs.stat(pack, function(err, stat) {
                    statPack = stat;
                    cb();
                });
            }], function(err) {


                callback(err, statPack.mtime >= statOrig.mtime);
            });
        },
        css: function(filePath, forcePackage, callback) {
            var me = this;
            var filename = filePath.replace(/(\\|\/|\:)/g, ''),
                _path = path.normalize([packageDir, filename].join(''));
            fs.exists(_path, function(exists) {
                if (!forcePackage && exists) {

                    me.checkFileStat(filePath, _path, function(err, good) {
                        if (good) {

                            callback(null, _path);
                        } else {
                            me.css(filePath, true, callback);
                        }
                    })

                } else {
                    if (!packagerCfg.onDemand) {
                        callback(null, filePath);
                        return;
                    }

                    var source,
                        result;
                    async.series([function(cb) {
                        fs.readFile(filePath, 'utf8', function(err, fileData) {
                            source = fileData;
                            cb(err);
                        });
                    }, function(cb) {
                        cleanCSS.minify(source, function(err, minified) {
                            result = minified && minified.styles;
                            cb(err);
                        });
                    }, function(cb) {
                        fs.writeFile(_path, result, function(err) {
                            cb(err);
                        });
                    }], function(err) {
                        callback(err, err && filePath || _path);
                    });
                }
            });
        },
        js: function(filePath, forcePackage, callback) {
            var me = this;
            var filename = filePath.replace(/(\\|\/|\:)/g, ''),
                _path = path.normalize([packageDir, filename].join(''));
            fs.exists(_path, function(exists) {
                if (!forcePackage && exists) {
                    me.checkFileStat(filePath, _path, function(err, good) {
                        if (good) {
                            callback(null, _path);
                        } else {
                            me.js(filePath, true, callback);
                        }
                    })
                } else {
                    if (!packagerCfg.onDemand) {
                        callback(null, filePath);
                        return;
                    }
                    var source,
                        result;
                    async.series([function(cb) {
                        fs.readFile(filePath, 'utf8', function(err, fileData) {
                            source = fileData;
                            cb(err);
                        });
                    }, function(cb) {

                        var minifyResult = UglifyJS.minify(source, {
                            fromString: true
                        });
                        result = minifyResult.code;
                        cb();
                    }, function(cb) {
                        fs.writeFile(_path, result, function(err) {
                            cb(err);
                        });
                    }], function(err) {
                        callback(err, err && filePath || _path);
                    });
                }
            });
        }
    }

});