Core.define('common.Utils', {
	singleton: true,
	arrayContains: function(array, obj) {
		var i = array.length;
		while (i--) {
			if (array[i] === obj) {
				return true;
			}
		}
		return false;
	}
});