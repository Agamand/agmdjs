var global = this;
global.Core = {
	clazz: {},
	_data: {},
	fileLoaded: {},
	currentNamespace: []
}
global.g = global;


var _location = window.location.pathname;
_location = _location.split('/');
_location.pop();
_location.push('js');
Core.jsPath = {
	'AgmdJS': '/res/agmdjs/js/class'
}

var Class = function() {};

//default Base class function

Class.prototype = {
	/*log: function() {
		var args = Array.splice(arguments, 1, 0, this.namespace);

		Core.log_ns(args);
	},
	info: function() {
		Core.Logger.info_ns(this.namespace, arguments);
	},
	debug: function() {
		Core.Logger.debug_ns(this.namespace, arguments);
	},
	warning: function() {
		Core.Logger.warning_ns(this.namespace, arguments);
	},
	critical: function() {
		Core.Logger.critical_ns(this.namespace, arguments);
	},
	error: function() {
		Core.Logger.error_ns(this.namespace, arguments);
	}*/
};


var initializing = false,
	fnTest = /xyz/.test(function() {
		xyz;
	}) ? /\b_super\b/ : /.*/;

// The base Class implementation (does nothing)
Class.extend = function(prop) {
	var _super = this.prototype;
	initializing = true;
	var prototype = new this();
	initializing = false;
	for (var name in prop) {; //console.log(name, typeof prop[name] == "function", typeof _super[name] == "function", fnTest.test(prop[name]))
		prototype[name] = typeof prop[name] == "function" && typeof _super[name] == "function" && fnTest.test(prop[name]) ? (function(name, fn) {
			return function() {
				var tmp = this._super;
				this._super = _super[name];; //console.log('call', name);
				var ret = fn.apply(this, arguments);
				this._super = tmp;
				return ret;
			};
		})(name, prop[name]) : prop[name];
	}

	function Class() {

		for (var i in this.__proto__) {
			var cur = this.__proto__[i];
			if (i == 'extendOf' || i == 'requires')
				continue;
			if (cur instanceof Function) continue;

			this[i] = Core.__eval(cur);
		}
		if (!initializing && this.ctor) this.ctor.apply(this, arguments);
	}
	Class.prototype = prototype;
	Class.constructor = this;
	Class.prototype.constructor = Class;
	Class.extend = arguments.callee;

	return Class;
};

Core.isDefined = function(value) {
	return typeof 'undefined' !== value && null != value;
}

Core.apply = function(object, config) {
	for (var i in config) {
		object[i] = config[i];
	}
	return object;
};

Core.copy = function(object, config, ignoreFunction) {
	for (var i in config) {
		var val = Core.__eval(config[i], ignoreFunction);
		if (Core.isDefined(val))
			object[i] = val;
	}
	return object;
};

Core.__eval = function(src, ignoreFunction) {
	if (src instanceof Function) {
		return !ignoreFunction && src || undefined;
	} else if (src instanceof Array) {
		var result = [];
		for (var j = 0, len = src.length; j < len; j++)
			result.push(Core.__eval(src[j]))
		return result;
	} else if (src instanceof Object && !(src instanceof Date)) {
		return Core.copy({}, src);
	} else if (src instanceof Date) {
		return new Date(src);
	} else
		return src;
};

Core.Async = {
	_series: function(tasks, end) {
		var result = [];
		//for (var i in tasks) {
		var i = 0;
		var next = function() {
			var current = tasks[i++];
			if (!current) {
				if (end)
					end(null, result);
			} else
				current(function(error, res) {
					if (error) {
						if (end)
							end(error);
					} else {
						result.push(res)
						next();
					}
				});
		}
		next();
	},
	series: function(tasks, callback) {
		var result = [],
			current = 0,
			next = function(err, data) {
				if (err) {
					callback(err);
					return;
				}
				if (tasks[current]) {
					tasks[current++](next);
				} else {
					callback(null, result);
				}
			}
		next();

	},
	parallel: function(tasks, callback) {
		var count = 0,
			result = [],
			onEnd = function(err, data) {
				result.push(data);
				if (!tasks[++count])
					callback(null, result);
			}
		for (var i in tasks) {
			task(onEnd);
		}
	}
}

Core.include = function(namespace, callback) {

	console.log('include', namespace);
	if (namespace instanceof Array) {
		var tasks = [];
		for (var i in namespace) {
			tasks.push((function(_namespace) {
				return function(callback) {
					Core.include(_namespace, callback);
				}
			})(namespace[i]));
		}
		Core.Async.series(tasks,
			function() {
				callback();
			});
		return;
	}

	var path = Core.getPath(namespace, '.js');; //console.log('load', namespace);
	//console.log(namespace, 'fail');
	if (!Core.clazz[namespace]) { 
		console.log(namespace, 'fail');
		Core.currentNamespace.push(namespace);
		//not supported for browser
		//require(path);

		Core.load(path, function() {
			callback(Core.clazz[namespace]);
		});
		return;
	}
	callback(Core.clazz[namespace]);
	//return Core.clazz[namespace]
}
Core.load = function(path, callback) {
	console.log('load', path)
	if (Core.fileLoaded[path])
		return;
	var d = document,
		js,
		ref = d.getElementsByTagName('script')[0];
	js = d.createElement('script');
	js.src = '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + path;
	ref.parentNode.insertBefore(js, ref);
	Core.fileLoaded[path] = true;
	$(js).on('load', function() {; //console.log("loadddddddd", path);
		if (callback) callback();
	})

	/*Core.Ajax.request({
		url: '//'+path,
		async: false,
		callback: function(response) {
			//;//console.log(response);
			try {
				var fnc = eval('(function(){' + response + '})();');
				me.fileLoaded[path] = true;
				callback()
			} catch (e) {
			;//console.log('loading fail', path);
			;//console.log(e);
			;//console.log(e.stack);
			;//console.log(response);
			}
		}
	})*/

	//console.log('sync ?', path);
}
Core._data = {};
Core.data = function(path, callback) {
	var me = this;
	if (Core._data[path]) {
		callback(Core._data[path]);
		return;
	}
	if (me.fileLoaded[path])
		return;
	Core.Ajax.request({
		url: '//' + window.location.hostname + (window.location.port ? ':' + window.location.port : '') + path,
		async: false,
		callback: function(response) {
			//;//console.log(response);
			try {
				Core._data[path] = JSON.parse(response);
				me.fileLoaded[path] = true;
				callback(Core._data[path]);
			} catch (e) {; //console.log('loading fail', path);
				; //console.log(e);
				; //console.log(e.stack);
				; //console.log(response);
			}
		}
	})

	//console.log('sync ?', path);
}


Core.solveNamespace = function(namespace) {
	var namespace = namespace.split('.'),
		tmp = global;
	i = 0;
	for (; i < namespace.length - 1; i++)
		tmp[namespace[i]] = tmp[namespace[i]] || {};
	return tmp;
}

Core.solvePath = function(namespace) {
	if (namespace.indexOf('.') == 0)
		return namespace.replace(/\./g, '');
	var split = namespace.split('.');
	for (var i in split) {
		split[i] = Core.jsPath[split[i]] || split[i];
	}
	return split.join('/');

}
Core.getPath = function(namespace, ext) {
	return [Core.jsPath, '/', Core.solvePath(namespace), ext || ''].join('')
}

Core.register = function(namespace, clazz, singleton) {
	var namespace = namespace.split('.'),
		tmp = global;
	i = 0;; //console.log('register', namespace)
	for (; i < namespace.length - 1; i++)
		tmp = (tmp[namespace[i]] = tmp[namespace[i]] || {});
	return tmp[namespace[i]] = singleton ? new clazz() : clazz;
}

Core.Ajax = {
	getXhr: (function(method) {
		var options = [

				function() {
					return new XMLHttpRequest();
				},
				function() {
					return new ActiveXObject('MSXML2.XMLHTTP.3.0');
				},
				function() {
					return new ActiveXObject('MSXML2.XMLHTTP');
				},
				function() {
					return new ActiveXObject('Microsoft.XMLHTTP');
				}
			],
			i = 0,
			len = options.length,
			xhr;
		for (; i < len; ++i) {
			try {
				xhr = options[i];
				xhr();
				break;
			} catch (e) {}
		}
		return xhr;
	}()),
	/**
	 * url, callback, method, params
	 */
	request: function(cfg) {
		var xmlhttp = this.getXhr(),
			bComplete = false,
			sVars = '';
		if (!xmlhttp) return null;
		cfg.method = (cfg.method || "GET").toUpperCase();
		try {
			if (cfg.method == "GET") {
				if (Core.isDefined(cfg.params)) {
					var first = true;
					for (var p in cfg.params) {
						if (first) {
							first = false;
							sVars += '?'
						} else {
							sVars += '&'
						};
						sVars += p + '=' + cfg.params[p];
					}
				}
				if (undefined == cfg.async)
					cfg.async = true;
				xmlhttp.open(cfg.method, cfg.url + sVars, cfg.async);
				sVars = "";
			} else if (cfg.method && cfg.method.toUpperCase() == "POST") {
				var boundary = '-----------------------------' + Math.floor(Math.random() * Math.pow(10, 8)),
					content = [];
				if (cfg.multipart) {
					var params = cfg.params;
					for (var i in params) {
						content.push('--' + boundary);

						var mimeHeader = 'Content-Disposition: form-data; name="' + i + '"; ';
						if (params[i].filename)
							mimeHeader += 'filename="' + params[i].filename + '";';
						content.push(mimeHeader);

						if (params[i].type)
							content.push('Content-Type: ' + params[i].type);

						content.push('');
						content.push(params[i].content || params[i]);
					}
					content.push('--' + boundary + '--');
					sVars = content.join('\r\n');
				} else {
					var first = true;
					for (var i in cfg.params) {
						if (first) {
							first = false;
						} else {
							content.push('&');
						}
						content.push(i);
						content.push('=');
						content.push(JSON.stringify(cfg.params[i]));
					}
					sVars = content.join('');
				}

				xmlhttp.open(cfg.method, cfg.url, true);
				xmlhttp.setRequestHeader("Content-Type",
					cfg.multipart ? ("multipart/form-data; boundary=" + boundary) : "application/x-www-form-urlencoded");

			}
			xmlhttp.onreadystatechange = function() {
				if (xmlhttp.readyState == 4 && !bComplete) {
					bComplete = true;
					cfg.callback(xmlhttp.response || xmlhttp.responseText);
					//cfg.callback(xmlhttp.response);
				}
			};
			xmlhttp.send(sVars);
		} catch (e) {
			return false;
		}
		return true;
	}
};



Core.define = function() {

	var namespace, clazz;

	if (arguments.length < 1)
		return null;
	else if (arguments.length == 1) {
		clazz = arguments[0];
		namespace = Core.currentNamespace.pop();
	} else {
		clazz = arguments[1];
		namespace = arguments[0]
	}
	var proto;
	/*if(!clazz.ctor)
	{
		//create default ctor
		clazz.ctor = function()
		{
			if(this._super)
				this._super.apply(this,arguments);
		}
	}*/
	clazz.namespace = namespace;
	console.log('define namespace', clazz.namespace, Core.currentNamespace);
	var test = /\.(\w*)$/g.exec(clazz.namespace);
	clazz.className = test.length > 1 ? test[1] : 'noNamedClass';
	var tasks = [],
		dependencies = clazz.includes || clazz.requires;
	for (var i in dependencies)
		tasks.push((function(dependencie) {
			return function(callback) {
				Core.include(dependencie, callback)
			}
		})(dependencies[i]));
	if (clazz.extendOf)
		tasks.push(function(callback) {
			Core.include(clazz.extendOf, callback)
		});
	Core.Async.series(tasks, function() {
		if (clazz.extendOf) {
			var base = Core.clazz[clazz.extendOf];
			proto = base.extend(clazz);
		} else proto = Class.extend(clazz);; //console.log('register', namespace);

		; //console.log("define", clazz.namespace, '(singleton :', clazz.singleton, ')', proto);
		Core.clazz[clazz.namespace] = clazz.singleton ? new proto() : proto;
		Core.register(clazz.namespace, proto, clazz.singleton);
	});
}


Core.Number = {
	isNumeric: function(a) {
		return a + 0 == a;
	},
	isValidNumber: function(a) {
		return undefined != a && null != a && this.isNumeric(a);
	}
}

Core.Date = {
	isValidDate: function(a) {
		return !isNaN((new Date(a)).getTime());
	},
	minToMS: function(min) {
		return Core.Date.MINUTES_IN_MS * min
	}
}

Core.Date.MINUTES_IN_MS = 60 * 1000;
Core.Date.HOURS_IN_MS = Core.Date.MINUTES_IN_MS * 60;
Core.Date.DAY_IN_MS = Core.Date.HOURS_IN_MS * 24;


Core.Observable = {
	events: (function() {
		return {}
	})(),
	listen: function(listeners) {
		if (listeners)
			for (var p in listeners)
				this.on(p, listeners[p])
	},
	on: function(event, cfg) {
		if (event instanceof Object)
			for (var p in event)
				this._on(p, event[p]);
		else
			this._on(event, cfg);
	},

	__initOnce: function() {
		this.eventsOnce = this.eventsOnce || {};
	},
	onOnce: function(event, cfg) {
		this.__initOnce();
		this.eventsOnce[event] = this.eventsOnce[event] || [];
		if ('function' == typeof cfg) {
			cfg = {
				callback: cfg
			}
		}
		this.eventsOnce[event].push(cfg);
	},
	off: function(eventName, fnc) {
		if (!fnc) {
			delete this.events[eventName];
			return;
		}
		var arr = this.events[eventName];
		if (arr) {
			for (var i = 0; i < arr.length; i++) {
				if (fnc == arr[i].callback) {
					arr.splice(i, 1);
					break;
					//						i--;
				}
			}

		}
	},
	_on: function(event, cfg) {

		if (cfg instanceof Array) {
			for (var i = 0, len = cfg.length; i < len; i++) {
				this._on(event, cfg[i]);
			}
			return;
		}

		this.events[event] = this.events[event] || [];
		if ('function' == typeof cfg) {
			cfg = {
				callback: cfg
			}
		}
		this.events[event].push(cfg);
	},
	onCfg: function(cfg) {
		this.on(cfg.event, cfg);
	},
	fireEvent: function(event) {
		var cb = null,
			scope, me = this;
		this.__initOnce();
		if (event == 'click')
		; //console.log(this, Core.Observable);
		if (Core.isDefined(this.events[event])) {
			for (var i = 0; i < this.events[event].length; i++) {
				cb = this.events[event][i];
				scope = cb.scope || scope;
				var args = Array.prototype.slice.call(arguments, 1);
				args.unshift(me);
				cb.callback.apply(scope, args);
			}
		}
		if (Core.isDefined(this.eventsOnce[event])) {
			for (var i = 0; i < this.eventsOnce[event].length; i++) {
				cb = this.eventsOnce[event][i];
				scope = cb.scope || scope;
				var args = Array.prototype.slice.call(arguments, 1);
				args.unshift(me);
				cb.callback.apply(scope, args);
			}
			delete this.eventsOnce[event];
		}
	},

	merge: function(listenersA, listenersB) {
		var merged = {};
		for (var p in listenersA)
			merged[p] = [listenersA[p]];
		for (var p in listenersB) {
			merged[p] = merged[p] || [];
			merged[p].push(listenersB[p]);
		}
		return merged;
	}
};

var Base64 = {
	_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	encode: function(e) {
		var t = "";
		var n, r, i, s, o, u, a;
		var f = 0;
		e = Base64._utf8_encode(e);
		while (f < e.length) {
			n = e.charCodeAt(f++);
			r = e.charCodeAt(f++);
			i = e.charCodeAt(f++);
			s = n >> 2;
			o = (n & 3) << 4 | r >> 4;
			u = (r & 15) << 2 | i >> 6;
			a = i & 63;
			if (isNaN(r)) {
				u = a = 64
			} else if (isNaN(i)) {
				a = 64
			}
			t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
		}
		return t
	},
	decode: function(e) {
		var t = "";
		var n, r, i;
		var s, o, u, a;
		var f = 0;
		e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
		while (f < e.length) {
			s = this._keyStr.indexOf(e.charAt(f++));
			o = this._keyStr.indexOf(e.charAt(f++));
			u = this._keyStr.indexOf(e.charAt(f++));
			a = this._keyStr.indexOf(e.charAt(f++));
			n = s << 2 | o >> 4;
			r = (o & 15) << 4 | u >> 2;
			i = (u & 3) << 6 | a;
			t = t + String.fromCharCode(n);
			if (u != 64) {
				t = t + String.fromCharCode(r)
			}
			if (a != 64) {
				t = t + String.fromCharCode(i)
			}
		}
		t = Base64._utf8_decode(t);
		return t
	},
	_utf8_encode: function(e) {
		e = e.replace(/\r\n/g, "\n");
		var t = "";
		for (var n = 0; n < e.length; n++) {
			var r = e.charCodeAt(n);
			if (r < 128) {
				t += String.fromCharCode(r)
			} else if (r > 127 && r < 2048) {
				t += String.fromCharCode(r >> 6 | 192);
				t += String.fromCharCode(r & 63 | 128)
			} else {
				t += String.fromCharCode(r >> 12 | 224);
				t += String.fromCharCode(r >> 6 & 63 | 128);
				t += String.fromCharCode(r & 63 | 128)
			}
		}
		return t
	},
	_utf8_decode: function(e) {
		var t = "";
		var n = 0;
		var r = c1 = c2 = 0;
		while (n < e.length) {
			r = e.charCodeAt(n);
			if (r < 128) {
				t += String.fromCharCode(r);
				n++
			} else if (r > 191 && r < 224) {
				c2 = e.charCodeAt(n + 1);
				t += String.fromCharCode((r & 31) << 6 | c2 & 63);
				n += 2
			} else {
				c2 = e.charCodeAt(n + 1);
				c3 = e.charCodeAt(n + 2);
				t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
				n += 3
			}
		}
		return t
	}
}
Core.Base64 = Base64;

Core.stringToByteArray = function(str) {
	var array = new(window.Uint8Array !== void 0 ? Uint8Array : Array)(str.length);
	var i;
	var il;

	for (i = 0, il = str.length; i < il; ++i) {
		array[i] = str.charCodeAt(i) & 0xff;
	}

	return array;
}

Core.byteArrayToString = function(byteArray) {
	var result = [];
	for (var i = 0; i < byteArray.length; i++) {
		result.push(String.fromCharCode(parseInt(byteArray[i])));
	}
	return result.join('');
}

Core.define('AgmdJS.Action', {
	singleton: true,
	proceed: function() {}
});

$(document).ready(function() {
	var actions = $('[action]');
	actions.on('click', function() {
		var me = $(this);
		var className = me.attr('action');
		Core.include(className, function(clazz) {
			if (clazz)
				clazz.proceed();
			else console.error(className, 'doesn\'t exists ! :/');
		})
	})
});