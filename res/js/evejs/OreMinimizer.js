var normalOre = {
	18: 1,
	19: 1,
	20: 1,
	21: 1,
	22: 1,
	1223: 1,
	1224: 1,
	1225: 1,
	1226: 1,
	1227: 1,
	1228: 1,
	1229: 1,
	1230: 1,
	1231: 1,
	1232: 1
}


var rethanOre = {
	1230:1,
	1228:1,
	1224:1,
	20:1,
	1226:1
}


var zeroOre = {
	19: 1,
	22: 1,
	1223: 1,
	1225: 1,
	1229: 1,
	1232: 1
}


var oreToCompressed = {
	18: 28422,
	19: 28420,
	20: 28410,
	21: 28401,
	22: 28367,
	1223: 28388,
	1224: 28424,
	1225: 28391,
	1226: 28406,
	1227: 28416,
	1228: 28429,
	1229: 28397,
	1230: 28432,
	1231: 28403,
	1232: 28394
}



var compressRatio = {
	18: 0.15,
	19: 16,
	20: 0.19,
	21: 0.14,
	22: 3.08,
	1223: 6.11,
	1224: 0.16,
	1225: 7.81,
	1226: 0.15,
	1227: 0.07,
	1228: 0.19,
	1229: 1.03,
	1230: 0.15,
	1231: 0.16,
	1232: 3.27
}



//http://api.eve-marketdata.com/api/item_prices2.json?char_name=demo&type_ids=28432,28422,28420,28410,28401,28367,28388,28424,28391,28406,28416,28429,28397,8432,28403,28394&region_ids=10000002&buysell=s
var prices = {"emd":{"version":2,"currentTime":"2015-10-17T17:33:05Z","name":"item_prices","key":"buysell,typeID,regionID","columns":"buysell,typeID,regionID,price,updated","result":[{"row":{"buysell":"s","regionID":"10000002","typeID":"8432","price":"0","updated":"2015-10-17 17:33:05"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28367","price":558799,"updated":"2015-10-17 17:25:32"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28388","price":539111.32246308,"updated":"2015-10-17 12:54:20"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28391","price":488939.44926679,"updated":"2015-10-17 15:43:58"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28394","price":"0","updated":"2015-10-17 17:33:05"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28397","price":190999.95326782,"updated":"2015-10-17 17:30:47"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28401","price":80998.637417417,"updated":"2015-10-17 17:06:06"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28403","price":"0","updated":"2015-10-17 17:33:05"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28406","price":"0","updated":"2015-10-17 17:33:05"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28410","price":30038.585097857,"updated":"2015-10-17 17:08:01"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28416","price":"0","updated":"2015-10-17 17:33:05"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28420","price":506644.58240618,"updated":"2015-10-17 17:31:01"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28422","price":"0","updated":"2015-10-17 17:33:05"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28424","price":5999.9888141093,"updated":"2015-10-17 14:47:33"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28429","price":3232.9000261028,"updated":"2015-10-17 17:09:41"}},{"row":{"buysell":"s","regionID":"10000002","typeID":"28432","price":"0","updated":"2015-10-17 17:33:05"}}]}}



Core.define({
	ctor: function() {
		var me = this;
		Core.apply(me, Core.Observable);
		me.events = {};
		Core.async.series([
			function(cb){
				Core.data('/res/eam/db/eve-ore-reprocess-map.json', function(data) {
					me.oreReprocess = data;
					cb();
				})
			},
			function(cb){
				Core.data('/res/eam/db/eve-ore-items-map.json', function(data) {
					me.oreItem = data;
					cb();
				})
			},
			function(cb){
				Core.data('/res/eam/db/eve-mineral-items-map.json', function(data) {
					me.mineralItem = data;
					cb();
				})
			}
		],function(){
			me.onReady();
		})
	},
	onReady: function() {
		var me = this;
		if (!me.oreItem || !me.oreReprocess || !me.mineralItem) {
			return;
		}
		console.log('OreMinimizer ready');
		var minOre = {

		};
		console.log(me.mineralItem);
		for (var id in me.mineralItem) {
			minOre[id] = {};
			for (var oid in me.oreReprocess) {
				var o = me.oreReprocess[oid];
				//console.log(oid,me.oreReprocess[oid]);
				var v = 0;
				for (var i in o.reprocess) {
					if (+id == +o.reprocess[i].id)
						v = o.reprocess[i].quantity;
				}
				minOre[id][oid] = v;
			}
		}
		me.minOre = minOre;
		me.isReady = true;
		me.processPrice(prices);
		me.fireEvent('ready');
	},
	processPrice: function(prices) {
		var result = {},
			me = this;
		for (var i in prices.emd.result) {
			var r = prices.emd.result[i].row;
			result[r.typeID] = +r.price;
		}
		me.prices = result;
		console.log(me.prices);
	},
	solve: function(mineralRequired,options, callback) {
		var me = this;
		if (!me.isReady) {
			callback(true);
			return;
		}
		var lp = glp_create_prob();
		var result = {};
		/*

		Minimize
		obj: +17 x1_1 +23 x2_1 +16 x3_1 +19 x4_1 +18 x5_1 +21 x1_2 +16 x2_2 +20 x3_2 +19 x4_2 +19 x5_2 +22 x1_3 +21 x2_3 +16 x3_3 +22 x4_3 +15 x5_3 +18 x1_4 +16 x2_4 +25 x3_4 +22 x4_4 +15 x5_4 +24 x1_5 +17 x2_5
		+24 x3_5 +20 x4_5 +21 x5_5 +15 x1_6 +16 x2_6 +16 x3_6 +16 x4_6 +25 x5_6 +20 x1_7 +19 x2_7 +17 x3_7 +19 x4_7 +16 x5_7 +18 x1_8 +25 x2_8 +19 x3_8 +17 x4_8 +16 x5_8 +19 x1_9 +18 x2_9 +19 x3_9 +21 x4_9 +23 x5_9
		+18 x1_10 +21 x2_10 +18 x3_10 +19 x4_10 +15 x5_10 +16 x1_11 +17 x2_11 +20 x3_11 +25 x4_11 +22 x5_11 +22 x1_12 +15 x2_12 +16 x3_12 +23 x4_12 +17 x5_12 +24 x1_13 +25 x2_13 +17 x3_13 +25 x4_13 +19 x5_13 +24 x1_14
		+17 x2_14 +21 x3_14 +25 x4_14 +22 x5_14 +16 x1_15 +24 x2_15 +24 x3_15 +25 x4_15 +24 x5_15
		

		*/

		var ratio = options && options.refineRate || 0.716;
		var oreFilter = rethanOre;//zeroOre;
		var sources = ['Minimize'];
		var sub = ['obj:'];
		var i = 1;
		var xToOre = {};
		var oreToX = {};
		console.log(me.prices);
		for (var id in me.oreReprocess) {
			var x = 'x' + (i++)
			console.log(me.oreItem[id].attr);
			sub.push('+' + (me.oreItem[id].attr[161].value || 0), x);
			//sub.push('+' + (me.prices[oreToCompressed[id]] || 0), x);
			xToOre[x] = id;
			oreToX[id] = x;
		}
		sources.push(sub.join(' '), '', 'Subject To');
		i = 1;

		for (var mid in me.minOre) {
			var mo = me.minOre[mid];
			var cur = ['lim_' + (i++) + ':']
			var j = 1;

			for (var id in mo) {
				console.log(id,oreToCompressed[id],'include',((oreFilter[id]) ? 1 : 0),((me.prices[oreToCompressed[id]]) ? 1 : 0))
				cur.push('+' + mo[id] * ((oreFilter[id]) ? 1 : 0) * ratio, 'x' + (j++));
			}
			console.log('iiii', i, i - 2);
			cur.push('>=', Math.floor(mineralRequired[i - 2]));
			sources.push(cur.join(' '));
		}

		sources.push('', 'General');
		i = 1;
		var variables = []
		for (var id in me.oreReprocess) {
			variables.push('x' + (i++));
		}
		sources.push(variables.join(' '), ' ', 'End', '');
		console.log(sources.join('\n'))
		glp_read_lp_from_string(lp, null, sources.join('\n'));
		glp_scale_prob(lp, GLP_SF_AUTO);
		var smcp = new SMCP({
			presolve: GLP_ON
		});
		glp_simplex(lp, smcp);
		var iocp = new IOCP({
			presolve: GLP_ON
		});
		glp_intopt(lp, iocp);
		console.log("obj: " + glp_mip_obj_val(lp));
		var resultstr = []
		var xr = {};
		var cr = 0;
		var ucr = 0;
		var cost = 0;
		for (var i = 1; i <= glp_get_num_cols(lp); i++) {
			//console.log(glp_get_col_name(lp, i) + " -> " + glp_mip_col_val(lp, i));
			var x = glp_get_col_name(lp, i);
			var val = +glp_mip_col_val(lp, i);
			xr[x] = val;
			if (val > 0) {
				console.log(x, xToOre[x]);
				resultstr.push(me.oreItem[xToOre[x]].name + '\t' + val * 100);
				if (!compressRatio[xToOre[x]]) {
					console.log('fail compress', xToOre[x]);
				}
				console.log('m',me.oreItem[xToOre[x]].attr[161].value * val,me.oreItem[xToOre[x]].attr[161].value * val*100,ucr)
				ucr += me.oreItem[xToOre[x]].attr[161].value * val*100;
				cr += compressRatio[xToOre[x]] * val;
				if (!me.prices[oreToCompressed[xToOre[x]]]) {
					console.log('fail prices', xToOre[x]);
				}
				cost += (me.prices[oreToCompressed[xToOre[x]]] || 0) * val
			}
		}
		var rr = [];

		for (var i in me.minOre) {
			var mo = me.minOre[i];
			var a = 0;
			for (var j in mo) {
				a += xr[oreToX[j]] * mo[j] * ratio;
			}
			rr.push(a);
		}
		var diff =[]
		for(var i in rr)
		{
			diff.push(rr[i]-mineralRequired[i]);
		}

		console.log(resultstr.join('\n'));
		console.log(rr);
		console.log(cr, 'm3');
		console.log(cost, 'isk');
		result.str = resultstr.join('\n');
		result.cost = cost + ' isk';
		result.cargou = ucr + ' m3';
		result.cargo = cr + ' m3';
		result.diff = diff;
		console.log(result);
		if (callback) callback(null, result);
	}


})