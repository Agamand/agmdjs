Agmd.Number = {
	isNumeric: function(a) {
		return a + 0 == a;
	},
	isValidNumber: function(a) {
		return undefined !== a && null !== a && this.isNumeric(a);
	},
	getNearestPow2: function(value) { // only work on 32 first bits (max value 2147483648)
		--value;
		for (var i = 0; i <= 8; ++i) 
			value |= value >> (1 << i);
		return ++value;

	}
};