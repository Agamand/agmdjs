/*var route = [
	['//test//hello.html', url('/hello.html'), ['']],
	['//test//%^([1-9])+.html$', url('/super/$0.html')]
];*/
/**
 * Routing System V2
 * 
 */

this.Agmd = Agmd || {};
Agmd.RoutingTree = Agmd.RoutingTree || {};
Agmd.Routing = {
    url: function(_url, params) {
        var purl = Agmd.Routing.parsePatternUrl(_url);
        return function() {
            var rurl = [];
            var a = arguments;
            var j = 0;
            for (var i in purl) {
                if (i % 2) {
                    rurl.push(arguments[purl[i]]);
                } else rurl.push(purl[i]);
            }
            return {
                path: rurl.join('')
            };
        };
    },
    action_url: function(_url) {
        var purl = Agmd.Routing.parsePatternUrl(_url);
        return function() {
            var rurl = [];
            var a = arguments;
            var j = 0;
            for (var i in purl) {
                if (i % 2) {
                    rurl.push(arguments[purl[i]]);
                } else rurl.push(purl[i]);
            }
            return {
                path: rurl.join('').replace(/\//g, '.')
            };
        };
    },
    action: function(act, args) {
        return function() {
            var rurl = [];
            var a = arguments;
            var j = 0;
            var _args = {};
            for (var i in args) {
                if (typeof args[i] == 'string' && args[i][0] == '$')
                    _args[i] = arguments[+args[i].substring(1)];
                else
                    _args[i] = args[i];
            }
            return {
                path: act,
                query: _args
            };
        };
    },
    resource: function(_url) {
        var purl = Agmd.Routing.parsePatternUrl(_url);
        return function() {
            var rurl = [];
            var a = arguments;
            var j = 0;
            for (var i in purl) {
                if (i % 2) {
                    rurl.push(arguments[purl[i]]);
                } else rurl.push(purl[i]);
            }
            return {
                path: 'AgmdJS.template.Resources',
                query: {
                    file: rurl.join('')
                }
            };
        };
    },
    service: function(serviceName) {
        return function() {
            return {
                service: serviceName
            };
        };
    },

    restAction: function(restAction, args) {
        return function() {
            var rurl = [];
            var a = arguments;
            var j = 0;
            var _args = {};
            for (var i in args) {
                if (typeof args[i] == 'string' && args[i][0] == '$')
                    _args[i] = arguments[+args[i].substring(1)];
                else
                    _args[i] = args[i];
            }
            return {
                "rest": restAction,
                args: _args
            };
        };
    },
    parsePatternUrl: function(patternUrl) {
        var reg = /\$([0-9]+)/g;
        var p = patternUrl.split(reg);
        var r = reg.exec(patternUrl);
        if (!r) return p;
        else {
            var purl = [];
            for (var i in p) {
                purl.push(p[i]);
                if (r[1 + i]) purl.push(r[1 + i]);
            }
            return purl;
        }
    },

    addRoute: function(from, to) {
        var me = this;
        me._addRoute(Agmd.RoutingTree, from, to);
    },
    createRouteTree: function(route) {
        var me = this,
            tree = {};
        if (route instanceof Array) {
            for (var i in route) {
                var c = route[i],
                    path = c[0]
                me._addRoute(tree, path, c[1])
            }
        } else if (route instanceof Object) {
            for (var path in route) {
                var c = route[path],
                    path = path
                me._addRoute(tree, path, c)
            }
        }

        return tree;
    },
    _addRoute: function(tree, path, result) {
        var ctree = tree;

        path = path.split('//');
        for (var j in path) {
            var part = path[j];
            if (!part.length) continue;
            if (part[0] == '%') {
                //regexp
                part = part.substr(1);
                var regexp = new RegExp("^" + part);
                ctree.regexp = ctree.regexp || {};
                var subTree = ctree.regexp[part] || {
                    test: regexp,
                    successTree: {}
                };
                ctree.regexp[part] = subTree;
                if (+j != path.length - 1)
                    ctree = subTree.successTree;
                else {
                    subTree.successTree = subTree.successTree || {};
                    subTree.successTree.$result = result;
                }
            } else {
                if (+j != path.length - 1)
                    ctree = ctree[part] = ctree[part] || {};
                else {
                    ctree[part] = ctree[part] || {};
                    ctree[part].$result = result;
                }
                ;
            }
        }
    },
    processPath: function(path, tree) {
        if (path == '/')
            path = '/index';
        var part = path.split('/');
        var node = tree,
            args = [],
            i,
            found = null,
            isEnding = false;
        for (var i = 0; i < part.length; i++) {
            var a = part[i];
            Logger.debug('route process part :', a);
            found = null;
            if (!a.length) continue; // ignore empty string
            if (node === null || typeof (node) != 'object') {
                Logger.error('disbranch :/', node);
                return false;
            }

            if (node[a]) {
                Logger.debug('simple node', node[a]);
                found = node[a];
            } else if (node.regexp) {


                var r = node.regexp;
                Logger.debug('try regexp node', r);
                var lastPart = [];
                for (var j = i, l = part.length; j < l; ++j)
                    lastPart.push(part[j]);
                lastPart = lastPart.join('/');
                var stop = false;
                isEnding = false;
                for (k in r) {
                    var regex = r[k];
                    Logger.debug('regexp node "', regex.test, '" apply on "', lastPart, '"');
                    var toTest = a;
                    var result = regex.test.exec(lastPart);
                    Logger.debug('regexp result', result);
                    if (result) {

                        for (var t = 1, max = result.length; t < max; t++)
                            args.push(result[t]);
                        var partRemain = lastPart.substring(result[0].length);
                        if (partRemain.length > 0) {
                            partRemain = partRemain.split('/');
                            i = part.length - partRemain.length;
                        } else {
                            isEnding = true;
                        }
                        found = regex.successTree;
                        break;
                    }
                }

            } else {
                return false;
            }
            node = found;
            if (isEnding)
                break;
        }
        if (node === null || typeof (node) != 'object' || typeof node.$result != 'function') {
            return false;
        } else {
            return node.$result.apply(null, args);
        }
    },
    route: function(path) {
        return this.processPath(path, Agmd.RoutingTree);
    }
};