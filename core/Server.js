var sys = require('sys'),
	url = require('url'),
	http = require('http'),
	qs = require('querystring'),
	async = require('async'),
	fs = require('fs'),
	path = require('path'),
	cluster = require('cluster'),
	numCPUs = require('os').cpus().length,
	HttpServer = Agmd.include('AgmdJS.core.HttpServer');
var arg_tags = {
	"--config": "configFile",
	"-c": "configFile",
	"-p": "port",
	"--port": 'port',
	"--cluster": 'cluster',
	"-cl": "cluster",
	'--clusterCount': "clusterCount",
	'-cc': "clusterCount"
};
var args = {
	configFile: "./Config-default.js"
};
Agmd.define('AgmdJS.core.Server', {
	ctor: function(cfg) {
		if (cfg)
			this.configure(cfg);
	},
	configure: function(cfg) {
		var me = this;
		me.cfg = me.cfg || {};
		Agmd.apply(me.cfg, cfg);
	},
	start: function(onLoad) {
		var me = this;
		if (cluster.isMaster && me.cfg.cluster && me.cfg.cluster.enable) {
			// Fork workers.
			var count = cfg.cluster.count || numCPUs;
			for (var i = 0; i < count; i++) {
				cluster.fork();
			}
			cluster.on('exit', function(worker, code, signal) {
				cluster.fork();
			});
		} else {
			me.info('server START');
			var server = new HttpServer({
				port: me.cfg.port || 80,
			}).start();
		}
	}

});