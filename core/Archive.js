const Path = require('path'),
	AdmZip = require('adm-zip');
var Archive = Agmd.Archive = {
	getExt: function(path) {
		path = path.split('.');
		return path[path.length - 1];
	},
	getArchive: function(path) {
		var ext = Archive.getExt(path);
		if (ext in Archive) {
			return Archive[ext].getArchive(path);
		} else {
			throw new Error(ext + ' file are not supported')
		}
	},
	createArchive: function(path) {
		var ext = Archive.getExt(ext);
		if (ext in Archive) {
			return Archive[ext].createArchive(path);
		} else {
			throw new Error(ext + ' file are not supported')
		}
	},
	zip: {
		getArchive: function(path) {
			return new Zip(path, true);
		},
		createArchive: function(path) {
			return new Zip(path);
		}
	}
};



var Zip = function(path, exist) {
	var me = this;
	me.path = path;
	me.exist = exist;
}
Zip.prototype = {
	open: function() {
		var me = this;
		if (me.exist)
			me.zip = new AdmZip(me.path);
		else me.zip = new AdmZip();
		return me;
	},
	extract: function(where) {
		var me = this;
		if (!me.zip) {
			return me;
		}
		me.zip.extract(where);
		return me;
	},
	extractAll: function(where) {
		var me = this;
		if (!me.zip) {
			return;
		}
		me.zip.extractAll(where);
		return;
	},
	getEntries: function() {
		var me = this;
		if (!me.zip) {
			return me.zip;
		}
		var zipEntries = me.zip.getEntries();
		var data = [];
		zipEntries.forEach(function(zipEntry) {
			data.push(zipEntry.entryName);
		});
		return data;
	},
	readEntry: function(entryPath) {
		var me = this;
		if (!me.zip) {
			return "fail";
		}
		return me.zip.readFile(entryPath);
	},
	readEntryAsText: function(entryPath) {
		var me = this;
		if (!me.zip) {
			return "fail";
		}
		return me.zip.readAsText(entryPath);
	},
	addFile: function(path, buffer) {
		var me = this;
		if (!me.zip) {
			return me.zip;
		}
		zip.addFile(path, typeof buffer == "string" ? new Buffer(buffer) : buffer);
		return me;
	},
	addLocalFile: function(path) {
		var me = this;
		if (!me.zip) {
			return me;
		}
		me.addLocalFile(path);
		return me;
	},
	write: function(path) {
		var me = this;
		if (!me.zip) {
			return;
		}
		me.zip.writeZip(path);
	}
}