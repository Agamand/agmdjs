var util = require('util'),
    fs = require('fs'),
    colors;
try {
    colors = require('colors');
} catch (e) {}
var Logger = function() {
    var me = this;
    me.DEBUG = 'DEBUG';
    me.INFO = 'INFO';
    me.WARNING = 'WARNING';
    me.CRITICAL = 'CRITICAL';
    me.ERROR = 'ERROR';
    me.filter = {
        DEBUG: 1,
        INFO: 1,
        WARNING: 1,
        CRITICAL: 1,
        ERROR: 1,
        HTTP: 1,
        HTTPCLIENT: 1
    };

    me.COLOR = {
        DEBUG: 'green',
        WARNING: 'yellow',
        CRITICAL: 'red',
        INFO: 'white',
        ERROR: 'red',
        HTTP: 'magenta',
        HTTPCLIENT: 'cyan',
        DEFAULT: 'white',
    };
    me.file = {};
};
Logger.prototype = {


    /**
     ** log(type,text0, ..., textn)
     **/
    log: function( /*type,text0,text1,...,textn*/ ) {

        var me = this;
        if (me.disable)
            return;
        var type;
        if (arguments.length < 2)
            return;
        type = arguments[0];
        if (!me.filter[type])
            return;
        var buffer = new Array(arguments.length - 1);
        for (var i = 1, len = arguments.length; i < len; i++) {
            /*var value = typeof arguments[i] == "object" ? util.inspect(arguments[i], {
            	depth: 1
            }) : arguments[i];*/
            buffer[i - 1] = arguments[i];
        }
        var buf = [];
        if (me.showtime)
            buf.push(('[' + (new Date().toString()) + '][' + type + '] :'), buffer.join(' '));
        else
            buf.push(('[' + type + '] :'), buffer.join(' '));


        if (colors)
            console.log(buf.join(' ')[me.COLOR[type] || me.COLOR.DEFAULT]);
        else console.log(buf.join(' '));

        if (me.file[type]) {
            me.writeFile(type, buf.join(' '));
        }
    },
    log_ns: function( /*type,namespace,text0,text1,...,textn*/ ) {
        var me = this;
        if (me.disable)
            return;
        var type,
            namespace;
        if (arguments.length < 3)
            return;
        type = arguments[0];
        namespace = arguments[1];

        if (!me.filter[type])
            return;
        var buffer = new Array(arguments.length - 2);
        for (var i = 2, len = arguments.length; i < len; i++) {
            var value = typeof arguments[i] == "object" ? util.inspect(arguments[i], {
                depth: null
            }) : arguments[i];
            buffer[i - 1] = value;
        }
        var buf = [];
        if (me.showtime)
            buf.push(('[' + (new Date().toString()) + '][' + type + '][' + namespace + '] :'), buffer.join(' '));
        else
            buf.push(('[' + type + '][' + namespace + '] :'), buffer.join(' '));
        if (colors)
            console.log(buf.join(' ')[me.COLOR[type] || me.COLOR.DEFAULT]);
        else console.log(buf.join(' '));
    },
    countEntry: function(path) {
        if (fs.existsSync(path)) {
            var data = fs.readFileSync(path, 'utf8');
            data = data.split('\n');
            return data.length;
        }
        return 0;
    },
    writeFile: function(type, buffer) {
        //first check if file is not open
        var me = this;
        me._file = me._file || {};
        me._file[type] = me._file[type] || {};
        if (!me._file[type].fd, !me._file[type].fd) {
            Core.Fs.makePathSync(me.file[type].path);
            if (me.file[type].limitEntry) {
                me._file[type].maxEntry = me.file[type].limitEntry;
                me._file[type].currentEntry = me.countEntry(me.file[type].path);
            }
            me._file[type].fd = fs.openSync(me.file[type].path, 'a+');
        }

        fs.writeSync(me._file[type].fd, buffer);
        fs.writeSync(me._file[type].fd, "\n");
        if (me._file[type].maxEntry)
            me._file[type].currentEntry++;
        if (me._file[type].currentEntry >= me._file[type].maxEntry) {
            fs.closeSync(me._file[type].fd);
            delete me._file[type].fd;
            var newPath = me.file[type].path + '.01';
            if (fs.existsSync(newPath)) {
                fs.unlinkSync(newPath);
            }
            fs.renameSync(me.file[type].path, newPath);
        }
    },

    getLog: function(type, count) {
        var me = this;
        if (me.file[type] && me.file[type].path) {
            if (fs.existsSync(me.file[type].path)) {

                var data = fs.readFileSync(me.file[type].path, 'utf8');
                data = data.split('\n');

                if (data.length < count) {
                    var neededCount = count - data.length;
                    if (fs.existsSync(me.file[type].path + '.01')) {
                        var oldData = fs.readFileSync(me.file[type].path + '.01', 'utf8');
                        oldData = oldData.split('\n');
                        data = oldData.concat(data);
                    }
                }

                if (data.length > count)
                    data.splice(0, data.length - count);
                return data;
            }
        }
    },
    //wrappers functions

    logWrapper: function(type, args, ns) {
        var me = this;
        args = Array.prototype.slice.call(args);
        if (ns)
            args.splice(0, 0, type, ns);
        else args.splice(0, 0, type);
        if (ns)
            me.log_ns.apply(me, args);
        else me.log.apply(me, args);
    },

    //function wrapper
    info: function() {
        var me = this;
        me.logWrapper(me.INFO, arguments);
    },
    debug: function() {
        var me = this;
        me.logWrapper(me.DEBUG, arguments);
    },
    warning: function() {
        var me = this;
        me.logWrapper(me.WARNING, arguments);
    },
    critical: function() {
        var me = this;
        me.logWrapper(me.CRITICAL, arguments);
    },

    error: function() {
        var me = this;
        me.logWrapper(me.ERROR, arguments);
    },

    //function wrapper with namespace
    info_ns: function(ns, args) {
        var me = this;
        me.logWrapper(me.INFO, args, ns);
    },
    debug_ns: function(ns, args) {
        var me = this;
        me.logWrapper(me.DEBUG, args, ns);
    },
    warning_ns: function(ns, args) {
        var me = this;
        me.logWrapper(me.WARNING, args, ns);
    },
    critical_ns: function(ns, args) {
        var me = this;
        me.logWrapper(me.CRITICAL, args, ns);
    },

    error_ns: function(ns, args) {
        var me = this;
        me.logWrapper(me.ERROR, args, ns);
    },
    setFilter: function(filter) {
        this.filter = filter || this.filter;
    },
    addFilter: function(filter) {
        if (!filter)
            return;
        this.filter[filter] = 1;
    },
    removeFilter: function(filter) {
        if (this.filter && filter)
            delete this.filter[filter];
    },
    configure: function(config) {
        var me = this;
        Core.applyConf(me, config);
    }
};

global.Logger = Agmd.Logger = new Logger();