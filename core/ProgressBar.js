var progress = Agmd.include('.progress');


var ProgressBar = Agmd.define('AgmdJS.Agmd.ProgressBar', {
	__INTERVAL: 100, //100ms
	ctor: function(cfg) {

	},


	start: function(max, title) {
		var me = this;
		if (me.__progressBar)
			me.complete(true); // you cannot start two progress bar :/ the old one will be complete.
		me.max = max;
		me.count = me.nextTick = 0;
		var progressStyle = 'processing [:bar] :percent :current/:total :etas';
		if (title)
			progressStyle = 'processing ' + title + ' [:bar] :percent :current/:total :etas';
		me.__progressBar = new progress(progressStyle, {
			complete: '=',
			incomplete: ' ',
			width: 20,
			total: me.max
		});
		me.__interval = setInterval(function() {
			me.__updateInterval();
		}, me.__INTERVAL);


	},
	__updateInterval: function() {
		var me = this;
		if (me.__progressBar && me.nextTick) {
			me.__progressBar.tick(me.nextTick);
			me.count += me.nextTick;
			me.nextTick = 0;

		}
	},

	update: function(count) {
		var me = this;
		if (!me.__progressBar)
			return;
		me.nextTick = (me.nextTick || 0) + count;
		if (me.count + me.nextTick >= me.max)
		{
			me.complete();
		}
	},
	complete: function(silent) {
		var me = this;

		me.stop(silent);

		me.__progressBar.tick(me.max - me.count);
		me.count = me.max;
		me.__progressBar = null;
		if (!silent)
			me.fireEvent('complete');
	},
	stop: function(silent) {
		var me = this;

		if (me.__interval)
			clearInterval(me.__interval);
		me.__interval = null;
		if (!silent)
			me.fireEvent('stop');
	}

});
Agmd.ProgressBar = new ProgressBar(); //singleton ProgressBar