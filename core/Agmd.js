var fs = require('fs'),
	path = require('path'),
	crypto = require('crypto');
Agmd = {
	clazz: {},
	_data: {},
	currentNamespace: [],
	__loaded: {},
	IsDebug: true,
	ClassLogger:true
},

Agmd._Deferred = require('jquery-deferred').Deferred;
Agmd.Deferred = Agmd._Deferred;

/**
 * @Deprecated using Core is deprecated, use Agmd instead
 */
Core = Agmd;

/*function() {

	var deferred = Agmd._Deferred();
	deferred.pipeTo = function(defer) {
		defer.done(this.resolve).fail(this.reject).progress(this.notify);
	}
	return deferred;
}*/

Agmd.Observable = {
	events: (function() {
		return {}
	})(),
	listen: function(listeners) {
		if (listeners)
			for (var p in listeners)
				this.on(p, listeners[p])
	},
	on: function(event, cfg) {
		if (event instanceof Object)
			for (var p in event)
				this._on(p, event[p]);
		else
			this._on(event, cfg);
	},

	__initOnce: function() {
		this.eventsOnce = this.eventsOnce || {};
	},
	once: function(event, cfg) {
		this.__initOnce();
		this.eventsOnce[event] = this.eventsOnce[event] || [];
		if ('function' == typeof cfg) {
			cfg = {
				callback: cfg
			}
		}
		this.eventsOnce[event].push(cfg);
	},
	off: function(eventName, fnc) {
		if (!fnc) {
			delete this.events[eventName];
			return;
		}
		var arr = this.events[eventName];
		if (arr) {
			for (var i = 0; i < arr.length; i++) {
				if (fnc == arr[i].callback) {
					arr.splice(i, 1);
					break;
					//						i--;
				}
			}

		}
	},
	_on: function(event, cfg) {

		if (cfg instanceof Array) {
			for (var i = 0, len = cfg.length; i < len; i++) {
				this._on(event, cfg[i]);
			}
			return;
		}

		this.events[event] = this.events[event] || [];
		if ('function' == typeof cfg) {
			cfg = {
				callback: cfg
			}
		}
		this.events[event].push(cfg);
	},
	onCfg: function(cfg) {
		this.on(cfg.event, cfg);
	},
	fireEvent: function(event) {
		var cb = null,
			scope, me = this;
		this.__initOnce();
		if (Agmd.isDefined(this.events[event])) {
			for (var i = 0, l = this.events[event].length; i < l; i++) {
				cb = this.events[event][i];
				scope = cb.scope || scope;
				var args = Array.prototype.slice.call(arguments, 1);
				args.unshift(me);
				cb.callback.apply(scope, args);
			}
		}
		if (Agmd.isDefined(this.eventsOnce[event])) {
			var evts = this.eventsOnce[event];
			delete this.eventsOnce[event];
			for (var i = 0; i < evts.length; i++) {
				cb = evts[i];
				scope = cb.scope || scope;
				var args = Array.prototype.slice.call(arguments, 1);
				args.unshift(me);
				cb.callback.apply(scope, args);
			}

		}
	},

	merge: function(listenersA, listenersB) {
		var merged = {};
		for (var p in listenersA)
			merged[p] = [listenersA[p]];
		for (var p in listenersB) {
			merged[p] = merged[p] || [];
			merged[p].push(listenersB[p]);
		}
		return merged;
	}
};



var Class = function() {};

//default Base class function

Class.prototype = {
	ctor: function() {
		//_super
	}
};
if (Agmd.ClassLogger) {
	Class.prototype.log = function() {
		var args = Array.splice(arguments, 1, 0, this.namespace);

		Agmd.log_ns(args);
	};
	Class.prototype.info = function() {
		Agmd.Logger.info_ns(this.namespace, arguments);
	};
	Class.prototype.debug = function() {
		Agmd.Logger.debug_ns(this.namespace, arguments);
	};
	Class.prototype.warning = function() {
		Agmd.Logger.warning_ns(this.namespace, arguments);
	};
	Class.prototype.critical = function() {
		Agmd.Logger.critical_ns(this.namespace, arguments);
	};
	Class.prototype.error = function() {
		Agmd.Logger.error_ns(this.namespace, arguments);
	};
} else {
	Class.prototype.log = Class.prototype.info = Class.prototype.debug = Class.prototype.warning = Class.prototype.critical = Class.prototype.error = function() {};
}


var initializing = false,
	fnTest = /xyz/.test(function() {
		xyz;
	}) ? /\b_super\b/ : /.*/;

Class.extend = function(prop) {
	var _super = this.prototype;
	initializing = true;
	var prototype = new this();
	initializing = false;
	if (!prop['ctor']) // add default ctor
	{
		prop.ctor = function() {
			this._super();
		}
	}
	for (var name in prop) {
		var a = typeof prop[name] == "function" && typeof _super[name] == "function" && fnTest.test(prop[name]) ? (function(name, fn) {

			//Logger.debug('inheritance from', name);
			return function() {
				var tmp = this._super;
				this._super = _super[name];
				var ret = fn.apply(this, arguments);
				this._super = tmp;
				return ret;
			};
		})(name, prop[name]) : prop[name];
		prototype[name] = a;
	}

	function Class() {
		Agmd.apply(this, Agmd.Observable);
		this.events = {};
		if (!initializing && this.ctor) this.ctor.apply(this, arguments);

	}
	Class.prototype = prototype;
	Class.constructor = Class;
	Class.extend = arguments.callee;

	return Class;
};

Agmd.isDefined = function(value) {
	return typeof 'undefined' !== value && null != value;
}

Agmd.apply = function(elt) {
	for (var t = 1, n = arguments.length; n > t; t++)
		for (var i in arguments[t]) elt[i] = arguments[t][i];
	return elt;
}

Agmd.solveNamespace = function(namespace) {
	var namespace = namespace.split('.'),
		tmp = global,
		i = 0;
	for (; i < namespace.length - 1; i++)
		tmp[namespace[i]] = tmp[namespace[i]] || {};
	return tmp;
}

Agmd.solvePath = function(namespace) {
	if (namespace.indexOf('.') == 0)
		return namespace.replace(/\./g, '');
	//namespace.replace(/\./g, '/');
	var split = namespace.split('.');
	for (var i in split) {
		split[i] = this.__config.namespace[split[i]] || split[i];
	}
	return path.resolve(split.join('/'));
}
Agmd.getPath = function(namespace, ext) {
	return [Agmd.solvePath(namespace), ext || ''].join('')
}
Agmd.include = function(namespace, callback) {
	try {

		if (namespace.indexOf('.') == 0) {
			if (callback)
				callback(null, Agmd.clazz[namespace] || (Agmd.clazz[namespace] = Agmd.loadFile(Agmd.solvePath(namespace))))
			return Agmd.clazz[namespace] || (Agmd.clazz[namespace] = Agmd.loadFile(Agmd.solvePath(namespace)));
		}
		var cpath = Agmd.getPath(namespace, '.js');
		if (!Agmd.clazz[namespace]) {
			Agmd.currentNamespace.push(namespace);
			Agmd.loadFile(cpath);
			Agmd.currentNamespace.pop();
		}
		if (callback)
			callback(null, Agmd.clazz[namespace])
		return Agmd.clazz[namespace];
	} catch (e) {
		if(Logger)
			Logger.error("fail to include", namespace, e);
		if (callback)
			callback(e);
		return null;
	}
}
Agmd.loadFile = function(file) {
	var me = this,
		absPath = path.resolve(file);
	me.__loaded[absPath] = true;
	return require(file);
};
Agmd.data = function(namespace, data) {
	if (data)
		return Agmd._data[namespace] = data;

	var path = Agmd.getPath(namespace, '.json');
	if (!fs.existsSync(path)) {
		return null;
	}

	var data;
	try {
		data = Agmd._data[namespace] || (Agmd._data[namespace] = Agmd.loadFile(path));
	} catch (e) {
		data = null;
	}


	return data;
};

Agmd.loadFileFromPathOrNamespace = function(namespaceOrPath, ext, callback) {
	var path = namespaceOrPath;

	if (arguments.length < 3) {
		callback = ext;
		ext = null;
	}

	if (-1 == path.indexOf(/(\\|\/)/)) {
		path = Agmd.solvePath(path);

	}
	if (ext)
		path += ext;

	fs.exists(path, function(exists) {
		if (exists) {
			fs.readFile(path, 'utf8', callback);
		} else callback('file don\'t exists');
	})
};


Agmd.copy = function(obj) {
	if (typeof obj != "object" || !obj instanceof Array) {
		return obj;
	}
	var copy;
	if (obj instanceof Array) {
		copy = [];
		for (var i in obj)
			copy[i] = this.copy(obj[i]);
	} else if (obj instanceof Date) {
		copy = new Date(obj);
	} else {
		if (obj.constructor.name != 'Object') {
			copy = new obj.constructor();
		} else
			copy = {};
		for (var attr in obj) {
			if (obj.hasOwnProperty(attr)) copy[attr] = this.copy(obj[attr]);
		}
	}
	return copy;
}

Agmd.traverse = function(obj, namespace) {
	var tmp = obj;
	namespace = namespace.split('.');
	for (var i = 0, len = namespace.length; i < len && tmp; i++)
		tmp = tmp[namespace[i]];
	return tmp;
};

Agmd.setAt = function(obj, namespace, value) {
	var tmp = obj;
	namespace = namespace.split('.');
	var i = 0,
		len = namespace.length - 1;
	for (; i < len && tmp; i++) {
		tmp = tmp[namespace[i]] || (tmp[namespace[i]] = {});

	}
	return tmp[namespace[len]] = value;
}

Agmd.removeAt = function(obj, namespace) {


	var tmp = obj,
		h = [tmp];
	namespace = namespace.split('.');
	for (var i = 0, len = namespace.length - 1; i < len && tmp; i++) {
		tmp = tmp[namespace[i]];
		h.push(tmp);
	}
	if (h.length != namespace.length)
		return false;
	for (var i = 0, l = h.length; i < l; ++i) {
		var j = h.length - 1 - i;
		delete h[j][namespace[j]];
		var k = Object.keys(h[j]);
		if (k.length)
			break;
	}

	return true;
}

Agmd.getFullyQualifiedObject = function(obj, prefix, result, root) {
	var res = result || {},
		prefix = prefix || '';
	if (null == root)
		root = true;
	for (var key in obj) {
		if (null == obj[key] || undefined == obj[key])
			continue;
		if (obj[key] instanceof Object && !(obj[key] instanceof Array) && !(obj[key] instanceof Date) && !(obj[key] instanceof RegExp) && (!obj[key].id || 'ObjectID' != obj[key]._bsontype))
			Agmd.getFullyQualifiedObject(obj[key], prefix + (root ? '' : '.') + key, res, false);
		else
			res[prefix + (root ? '' : '.') + key] = obj[key];
	}
	return res;
};

Agmd.register = function(namespace, clazz, asSingleton) {
	var namespace = namespace.split('.'),
		tmp = global;
	i = 0;
	for (; i < namespace.length - 1; i++)
		tmp = (tmp[namespace[i]] = tmp[namespace[i]] || {});
	return tmp[namespace[i]] = asSingleton ? new clazz() : clazz;
}

Agmd.applyConf = function(dest, cfg) {
	var me = this;

	if (cfg instanceof Object) {
		for (var i in cfg) {
			if (cfg[i] instanceof Object && !(cfg[i] instanceof Array) && !(typeof cfg[i] == 'function')) {
				if (!dest[i])
					dest[i] = {};
				me.applyConf(dest[i], cfg[i]);
			} else {
				dest[i] = cfg[i];
			}
		}
	}

};

Agmd.config = Agmd.configure = function(newconfig) {
	var me = this;
	me.__config = me.__config || {};
	if (newconfig) {
		if (newconfig instanceof String) {
			require(newconfig);
		} else if (newconfig instanceof Object) {
			me.applyConf(me.__config, newconfig);
		}

		if (Agmd.Logger && newconfig.logger)
			Agmd.Logger.configure(newconfig.logger);
	}
};

Agmd.getConfig = function(namespace,defaultValue) {
	defaultValue = defaultValue || null;
	if (!this.__config)
		return defaultValue;
	return Agmd.traverse(this.__config, namespace) || defaultValue;
};

Agmd.define = function(namespace, clazz) {


	if (arguments.length > 1) {
		clazz.namespace = namespace;
	} else {
		clazz = namespace;
		clazz.namespace = Agmd.currentNamespace[Agmd.currentNamespace.length - 1];
	}
	var proto;
	if (clazz.namespace) {
		var test = /\.(\w*)$/g.exec(clazz.namespace);

		clazz.className = test.length > 1 ? test[1] : 'noNamedClass';
	} else {
		clazz.className = "Anonymous";
	}
	if (clazz.requires) {

		var requires = clazz.requires;
		if (!(requires instanceof Array))
			requires = [requires];

		for (var i in clazz.requires)
			Agmd.include(clazz.requires[i]);
	}

	if (clazz.extendOf) {
		var extend = clazz.extendOf;
		/*		if (!(extend instanceof Array))
					extend = [extend];
				for (var i in extend) { //allow multi class Inheritance
					var base = Agmd.include(clazz.extendOf[i]);
					proto = base.extend(clazz);
				}*/

		if (extend instanceof Array)
			extend = extend[0]; // no multi inheritance for now

		var base = Agmd.include(clazz.extendOf);

		proto = base.extend(clazz);
	} else proto = Class.extend(clazz);
	var created = clazz.singleton ? new proto() : proto;
	if (clazz.namespace) {
		Agmd.clazz[clazz.namespace] = created
		Agmd.register(clazz.namespace, proto, clazz.singleton);
	}
	return created;
}
Agmd.Service = {
	__instance: {},
	getInstance: function(className, opt) {
		var me = this;
		var hash = "default";
		if (opt) {
			hash = crypto.createHash('md5').update(JSON.dstringify(opt)).digest('hex');
		}
		if (me.__instance[className] && me.__instance[className][hash])
			return me.__instance[className][hash];
		var clazz = Agmd.include(className);
		if (!clazz) {
			Logger.error('getInstance failed err :', ' clazz : ', clazz);
			return null;
		}
		me.__instance[className] = me.__instance[className] || {};
		return me.__instance[className][hash] = new clazz(opt || null);
	}
}

Agmd.Database = {
	currentConnector: null,
	getCurrentConnector: function() {
		return this.currentConnector;
	},
	setConnector: function(connector) {
		this.currentConnector = connector;
	}
}



Agmd.Filter = {
	convert: function(filter) {
		return Agmd.getFullyQualifiedObject(filter);
	},
	has: function(filters, data) {

		var me = this;
		for (var i in filters) {
			var filter = i.split('.'),
				v = filters[i];
			if (!me.operator.$apply(filter, data, v))
				return false;
		}

		return true;
	},
	operator: {
		'$or': function(f, data, v) {

			for (var i in v) {
				var f2 = Agmd.Filter.convert(v[i]);
				if (Agmd.Filter.has(f2, data))
					return true;
			}
			return false;

		},
		'$in': function(f, data, v) {
			for (var i in v) {

				if (Agmd.Filter.equals(data, v[i])) {

					return true;
				}
			}
			return false;
		},
		'$apply': function(f, data, v) {
			var tmp = data,
				operator = Agmd.Filter.operator;
			//Logger.debug(f, data, v)
			for (var i = 0, l = f.length; i < l && tmp; ++i) {
				if (tmp instanceof Array) {
					for (var j in tmp) {
						if (operator.$apply(f.slice(i), tmp[j], v))
							return true;
					}
					return false;
				} else if (operator[f[i]]) {
					return operator[f[i]](f.slice(i + 1), tmp, v)
				}
				tmp = tmp[f[i]];

			}
			if (tmp instanceof Array) {
				for (var j in tmp) {
					if (Agmd.Filter.equals(tmp[i], v))
						return true;
				}
				return false;
			} else return Agmd.Filter.equals(tmp, v);
		}
	},
	equals: function(value, fvalue) {
		if (fvalue instanceof RegExp)
			return fvalue.test(value);

		return fvalue == value;
	}

};
Agmd.Projection = {
	convert: function(proj) {
		return Agmd.getFullyQualifiedObject(proj);
	},
	project: function(proj, data) {

		var exclusive = false;
		var result = {};
		for (var i in proj) {
			if (proj[i]) {
				exclusive = true;
				break;
			}
		}
		if (exclusive) {
			for (var i in proj) {

				var value;
				if (proj[i] && (value = Agmd.traverse(data, i)) != undefined) {

					Agmd.setAt(result, i, value);
				}
			}
		} else {
			result = Agmd.copy(data);
			for (var i in proj) {
				var value;
				if (proj[i] == 0) {
					Agmd.removeAt(result, i);
				}
			}
		}

		return result;
	}
};



String.prototype.decompose = function() {

	var r = [];
	var x = 0,
		y = -1,
		n = 0,
		i, j;

	while (i = (j = this.charAt(x++)).charCodeAt(0)) {
		var m = (i == 46 || (i >= 48 && i <= 57));
		if (m !== n) {
			r[++y] = "";
			n = m;
		}
		r[y] += j;
	}

	return r;
}

Agmd.alphanumCompare = function(a, b, caseInsensitive) {

	a = a.decompose();
	b = b.decompose();
	for (var x = 0, aa, bb;
		(aa = a[x]) && (bb = b[x]); x++) {
		if (caseInsensitive) {
			aa = aa.toLowerCase();
			bb = bb.toLowerCase();
		}
		if (aa !== bb) {
			var c = Number(aa),
				d = Number(bb);
			if (c == aa && d == bb) {
				return c - d;
			} else return (aa > bb) ? 1 : -1;
		}
	}
	return a.length - b.length;
}

var testFilter = function(a, b, filter) {
	var va = Agmd.traverse(a, filter),
		vb = Agmd.traverse(b, filter);

	var r;
	if (typeof va == typeof vb && typeof va == "string") {
		r = Agmd.alphanumCompare(va, vb)
	} else r = va - vb;

	return r > 0 ? 1 : (r < 0 ? -1 : 0)

}


Agmd.Array = {
	sort: function(arr, filter) {
		return arr.sort(function(a, b) {
			for (var i in filter) {
				var r = 0,
					inverser = filter[i] >= 0 && 1 || -1;

				if (r = testFilter(a, b, i))
					return inverser * r;
			}
		})

	}
}


var sortByKeys = function(obj) {
	if (typeof obj != 'object') {
		return obj;
	}
	var sorted = {};
	var keys = Object.keys(obj).sort();
	for (var i in keys) {
		sorted[keys[i]] = sortByKeys(obj[keys[i]]);
	}
	return sorted;
};


JSON.dstringify = Agmd.dstringify = function() {
	arguments[0] = sortByKeys(arguments[0]);
	return JSON.stringify.apply(this, arguments);
}



Agmd.config({
	namespace: {
		'AgmdJS': path.normalize(__dirname + '/../')
	}
});


var Base64 = {
	_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
	encode: function(e) {
		var t = "";
		var n, r, i, s, o, u, a;
		var f = 0;
		e = Base64._utf8_encode(e);
		while (f < e.length) {
			n = e.charCodeAt(f++);
			r = e.charCodeAt(f++);
			i = e.charCodeAt(f++);
			s = n >> 2;
			o = (n & 3) << 4 | r >> 4;
			u = (r & 15) << 2 | i >> 6;
			a = i & 63;
			if (isNaN(r)) {
				u = a = 64
			} else if (isNaN(i)) {
				a = 64
			}
			t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a)
		}
		return t
	},
	decode: function(e) {
		var t = "";
		var n, r, i;
		var s, o, u, a;
		var f = 0;
		e = e.replace(/[^A-Za-z0-9\+\/\=]/g, "");
		while (f < e.length) {
			s = this._keyStr.indexOf(e.charAt(f++));
			o = this._keyStr.indexOf(e.charAt(f++));
			u = this._keyStr.indexOf(e.charAt(f++));
			a = this._keyStr.indexOf(e.charAt(f++));
			n = s << 2 | o >> 4;
			r = (o & 15) << 4 | u >> 2;
			i = (u & 3) << 6 | a;
			t = t + String.fromCharCode(n);
			if (u != 64) {
				t = t + String.fromCharCode(r)
			}
			if (a != 64) {
				t = t + String.fromCharCode(i)
			}
		}
		t = Base64._utf8_decode(t);
		return t
	},
	_utf8_encode: function(e) {
		e = e.replace(/\r\n/g, "\n");
		var t = "";
		for (var n = 0; n < e.length; n++) {
			var r = e.charCodeAt(n);
			if (r < 128) {
				t += String.fromCharCode(r)
			} else if (r > 127 && r < 2048) {
				t += String.fromCharCode(r >> 6 | 192);
				t += String.fromCharCode(r & 63 | 128)
			} else {
				t += String.fromCharCode(r >> 12 | 224);
				t += String.fromCharCode(r >> 6 & 63 | 128);
				t += String.fromCharCode(r & 63 | 128)
			}
		}
		return t
	},
	_utf8_decode: function(e) {
		var t = "";
		var n = 0;
		var r = c1 = c2 = 0;
		while (n < e.length) {
			r = e.charCodeAt(n);
			if (r < 128) {
				t += String.fromCharCode(r);
				n++
			} else if (r > 191 && r < 224) {
				c2 = e.charCodeAt(n + 1);
				t += String.fromCharCode((r & 31) << 6 | c2 & 63);
				n += 2
			} else {
				c2 = e.charCodeAt(n + 1);
				c3 = e.charCodeAt(n + 2);
				t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
				n += 3
			}
		}
		return t
	}
}
Agmd.Base64 = Base64;


Agmd.include('AgmdJS.core.Number');
Agmd.include('AgmdJS.core.Date');
Agmd.include('AgmdJS.core.Logger');
Agmd.include('AgmdJS.core.Fs');
Agmd.include('AgmdJS.core.Route');
Agmd.include('AgmdJS.core.Request');
Agmd.include('AgmdJS.core.Async');
Agmd.include('AgmdJS.core.ProgressBar');
Agmd.include('AgmdJS.core.Archive');