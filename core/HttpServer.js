var url = require('url'),
    http = require('http'),
    qs = require('querystring'),
    async = require('async'),
    fs = require('fs'),
    path = require("path"),
    crypto = require('crypto'),
    zlib = require('zlib'),
    HttpResponse = require(__dirname + '/HttpResponse.js');
/*
Catch all exception.
*/
process.on('uncaughtException', function(err) {
    Logger.critical('uncaughtException :', err, err instanceof Array ? err[0].stack : err.stack);
    process.exit(-1);
});
Agmd.define('AgmdJS.core.HttpServer', {
    password: '1(Dp9ç1&@!.F/DF?.T<)OJNFK2',
    COOKIE_TTL: 1000 * 60 * 60 * 24 * 30 * 6, // 6 MONTH
    cipher: null,
    decipher: null,
    ctor: function(cfg) {
        var me = this;
        me.cfg = cfg;
        me.activeRequests = 0;
        me.md5 = crypto.createHash('md5');
        me.md5.update(me.password);
        me.key = me.md5.digest('binary');
        me.md5 = crypto.createHash('md5');
        me.md5.update(me.key);
        me.iv = new Buffer('0123456789012345', 'ascii');
        me.routingTree = cfg.routingTree || Agmd.RoutingTree;
    },
    start: function() {
        var me = this;
        me.server = http.createServer(function() {
            me.onRequest.apply(me, arguments);
        }).listen(me.cfg.port);
        me.server.on('error', function() {
            Logger.error('Connection err', ...arguments);
        });
        Logger.info('Server running at http://127.0.0.1:/', me.cfg.port);
    },
    createUserCookie: function(request, response, user, callback) {
        var me = this,
            usrStr = JSON.stringify(user),
            buffer = new Buffer(usrStr, 'utf8');
        var cipher = crypto.createCipheriv("aes-128-cbc", me.key, me.iv);
        var zipped;
        zlib.deflate(buffer, function(err, comp) {
            zipped = comp;
            result = [];
            result.push(cipher.update(zipped, 'binary', 'base64'));
            result.push(cipher.final('base64'));
            me.writeSessionCookie(request, response, result.join(''), callback);
        });
    // Write te cookie to the response header.
    },
    writeSessionCookie: function(request, response, value, callback) {
        var me = this;
        response.setHeader("Set-Cookie", ["AGSID=" + encodeURIComponent(value) + "; path=/; Expires=" + new Date(new Date().getTime() + me.COOKIE_TTL).toString()]);
        callback(null, request, response);
    },
    removeSessionCookie: function(request, response, callback) {
        var me = this;
        response.setHeader("Set-Cookie", ["AGSID=deleted; path=/; Expires=" + new Date(0).toString()]);
        callback(null, request, response);
    },
    readUserCookie: function(request, response, callback) {
        // Try to read header for a Cooky, 
        var me = this,
            result = [];
        if (request._COOKIES && request._COOKIES['AGSID'] && 'deleted' != request._COOKIES['AGSID']) {
            var decipher = crypto.createDecipheriv('aes-128-cbc', me.key, me.iv);
            try {
                result.push(decipher.update(decodeURIComponent(request._COOKIES['AGSID']), 'base64', 'binary'));
                result.push(decipher.final('binary'));
            } catch (e) {
                me.error(e);
                me.onError(request, response, {
                    msg: 'Cookie issue'
                });
            }
            zlib.inflate(new Buffer(result.join(''), 'binary'), function(err, def) {
                try {

                    request._USER = JSON.parse(def);

                    if (!request._USER._id) {
                        // Remove old cookie.
                        me.debug('removeSessionCookie');
                        process.exit(0);
                        me.removeSessionCookie(request, response, function() {
                            callback(null, request, response);
                        });
                    } else
                        callback(null, request, response);
                } catch (e) {
                    callback({
                        msg: 'BAD_AGSID_err:' + e
                    }, request, response);
                }
            });
        } else {
            // If no session cookies, just callback.
            callback(null, request, response);
        }
    },
    extractCookies: function(request, response) {
        // To Get a Cookie
        var cookies = {};
        if (request.headers.cookie) request.headers.cookie.split(';').forEach(function(cookie) {
                var parts = cookie.split('=');
                cookies[parts[0].trim()] = (parts[1] || '').trim();
            });
        request._COOKIES = cookies;
    },
    onRequest: function(request, response) {
        var me = this;
        response.startTime = new Date();
        var _response = new HttpResponse(request, response, me); // wrap response;
        _response.startTime = new Date();
        request._SERVER = me;
        me.extractCookies(request, _response);
        Logger.log('HTTP', 'REQUEST', request.method, request.url);
        me.readUserCookie(request, _response, function(err, request, response) {
            if (!err) me.proceedAction(request, response);
            else me.onError(request, response, err);
        });
    },
    proceedAction: function(request, response) {
        var me = this,
            url_parts = url.parse(request.url, true),
            action = url_parts.pathname;
        request._GET = url_parts.query;
        var routePath,
            routeResult = Agmd.Route.route(action, me.routingTree);

        Logger.debug('routeResult', routeResult, action);
        if (!routeResult) {
            if ("/favicon.ico" === action) {
                me.onNotFound(request, response, true);
                return;
            }
            routePath = [(Agmd.getConfig('pathPrefix') || 'AgmdJS'), action.replace(/\//g, '.')].join('');
        } else if (routeResult.service) {
            try {
                var service = Agmd.Service.getInstance(routeResult.service);
                service.proceed(action, request, response);
            } catch (e) {
                me.onError(request, response, e);
            }
            return;
        } else if (routeResult.path) {
            routePath = routeResult.path;
            request._GET = Agmd.apply(request._GET || {}, routeResult.query);
        } else {
            me.onError(request, response, ["unhandle request path :", action].join(''));
            return;
        }
        me.debug('routePath', routePath, "from", request.headers['x-forwarded-for'] ||
            request.connection.remoteAddress ||
            request.socket.remoteAddress ||
            request.connection.socket.remoteAddress);

        if (-1 != action.indexOf("favicon.ico") || -1 != action.indexOf("favicon.png")) {
            me.onError(request, response, {});
            return;
        }
        Agmd.include(routePath, function(err, aclazz) {
            Logger.debug("include", typeof err, typeof aclazz);
            if (null === aclazz || undefined === aclazz || typeof (aclazz) != "function") {
                me.onError(request, response, {
                    msg: 'fail load ' + routePath
                });
                return;
            }
            //  One instance per request (share nothing strategy)
            var clazz = new aclazz();
            clazz.on('redirect', function(owner, redirectUrl) {
                me.onRedirect(request, response, redirectUrl);
            });
            clazz.on('error', function(owner, err) {
                me.onError(request, response, err, clazz);
            });
            clazz.on('notfound', function(owner, err) {
                Logger.debug('notfound proc !');
                me.onNotFound(request, response, err, clazz);
            });

            try {
                clazz.prepare(request, response);
            } catch (e) {
                me.onError(request, response, e);
            }
        });
    },
    onRedirect: function(request, response, redirectUrl) {
        response.writeHead(301, {
            Location: redirectUrl
        });
        response.end();
    },
    onSuccess: function() {},
    /* Default error to 503*/
    onError: function(request, response, err, clazz) {
        if (err) {
            Logger.error(new Date(), err);
            Logger.error('Stack : ', err.stack);
        } else {
            err = 'An unspecified error occurred.';
        }
        Logger.error(new Date(), err);
        response.writeHead(500, {
            'Content-Type': 'application/json',
        });
        response.end('{"success":false, "url" : \"' + request.url + '", "err":' + JSON.stringify(err.message || err.msg || err) + '}');

    },
    /* Default error to 404*/
    onNotFound: function(request, response, empty) {
        var me = this;
        if (!me.cfg || !me.cfg.staticPages || !me.cfg.staticPages.notFound) {
            response.writeHead(404, {
                'Content-Type': 'text/html',
            });
            response.end(empty ? '' : '<strong> 404 not found</strong>');
            return;
        }
        Agmd.include(me.cfg.staticPages.notFound, function(err, aclazz) {
            if (null === aclazz) {
                me.onError(request, response, err);
                return;
            }
            var clazz = new aclazz();
            clazz.on('error', function(owner, err) {
                me.onError(request, response, err, clazz);
            });
            try {
                clazz.prepare(request, response);
            } catch (e) {
                me.onError(request, response, e);
            }
        });
    }
});