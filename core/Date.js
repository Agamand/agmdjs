

var days = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];


var _Date = Agmd.Date = {
	isValidDate: function(a) {
		return !isNaN((new Date(a)).getTime());
	},
	minToMS: function(min) {
		return Agmd.Date.MINUTES_IN_MS * min;
	},
	formatTime: function(time) {
		if (time > 9)
			return time;
		return "0" + time;
	},
	formatForHeader: function(date) {
		return [days[date.getUTCDay()], ", ", date.getUTCDate(), " ", months[date.getUTCMonth()], " ", date.getUTCFullYear(), " ", _Date.formatTime(date.getUTCHours()), ":", _Date.formatTime(date.getUTCMinutes()), ":", _Date.formatTime(date.getUTCSeconds()), " GMT"].join('');
	}

};

Agmd.Date.MINUTES_IN_MS = 60 * 1000;
Agmd.Date.HOURS_IN_MS = Agmd.Date.MINUTES_IN_MS * 60;
Agmd.Date.DAY_IN_MS = Agmd.Date.HOURS_IN_MS * 24;