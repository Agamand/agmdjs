var Fs = Agmd.Fs = Agmd.FileSystem = {},
    fs = require('fs'),
    os = require('os');
Fs.fs = fs;

if (os.platform() != 'win32') {
    Fs.makePathSync2 = function (path, options) {

        path = path.split('/');
        var p = __dirname + '/../';

        var mode = (options && options.mode) || "0777";
        var owner = (options && options.owner) || null;
        for (var i = 0, len = path.length - 1; i < len; i++) {
            if (!path[i].length) 
                continue;
            p += '/' + path[i];
            if (!fs.existsSync(p)) {

                fs.mkdirSync(p, mode);
            }
        }
    };

    Fs.makePathSync = function (path, options) {
        path = path.split('/');
        var p = "";
        var mode = (options && options.mode) || "0777";
        var owner = (options && options.owner) || null;
        for (var i = 0, len = path.length - 1; i < len; i++) {
            if (!path[i].length) 
                continue;
            p += '/' + path[i];
            if (!fs.existsSync(p)) {
                fs.mkdirSync(p, mode);
                //if(owner) fs.chown(p,owner);
            }
        }
    };
} else {
    Fs.makePathSync2 = function (path, options) {

        path = path.split('/');
        var p = __dirname + '/../';

        var mode = (options && options.mode) || "0777";
        var owner = (options && options.owner) || null;
        for (var i = 0, len = path.length - 1; i < len; i++) {
            if (!path[i].length) 
                continue;
            p += '/' + path[i];
            if (!fs.existsSync(p)) {

                fs.mkdirSync(p, mode);
            }
        }
    };

    Fs.makePathSync = function (path, options) {
        path = path.split(/\\|\//);
        var p = path[0];

        var mode = (options && options.mode) || "0777";
        var owner = (options && options.owner) || null;
        for (var i = 1, len = path.length - 1; i < len; i++) {
            if (!path[i].length) 
                continue;
            p += '\\' + path[i];
            if (!fs.existsSync(p)) {
                fs.mkdirSync(p, mode);
                //if(owner) fs.chown(p,owner);
            }
        }
    };
}

Fs.getExt = function (path) {
    path = path.split('.');
    return path[path.length - 1];
};

Fs.getFilename = function (file) {
    path = path.split('.');
    return path.length > 1 && path.pop() && path.join('.') || path[0];
};