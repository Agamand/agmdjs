var Fs = Agmd.Async = {
	series: function(tasks, callback) {
		if (!tasks || !callback)
			return;
		var current = 0,
			next = function(err, data) {
				if (err) {
					callback(err);
					return;
				}

				if (tasks[current]) {
					tasks[current++](next);
				} else {
					callback();
				}
			};
		next();


	},
	parallel: function(tasks, callback) {
		if (!tasks || !callback)
			return;
		var count = 0,
			onEnd = function() {
				if (!tasks[++count])
					callback();
			};
		for (var i in tasks) {
			tasks[i](onEnd);
		}
	}
};