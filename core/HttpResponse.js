var zlib = require('zlib');
var HttpResponse = function(request, serverResponse, server) {
	var me = this;
	me.resp = serverResponse;
	me.server = server;
	me.request = request;
	me.statusCode = me.resp.statusCode;
	me.headersSent = me.resp.headersSent;
	me.sendDate = me.resp.sendDate;
	me.accept_encoding = request.headers['accept-encoding'] || '';

	if (Agmd.getConfig('http.gzip')) {
		/*if (-1 != me.accept_encoding.indexOf('deflate')) { // don't work with any browser
			me.encoding = 'deflate';
			me.writer = zlib.createDeflate();
			me.writer.pipe(me.resp);
		} else*/
		if (-1 != me.accept_encoding.indexOf('gzip')) {
			me.encoding = 'gzip';
			me.writer = zlib.createGzip();
			me.writer.pipe(me.resp);
		} else {
			me.encoding = false;
			me.writer = me.resp;
		}
	} else {
		me.encoding = false;
		me.writer = me.resp;
	}
};


var HeadersFormatter = {
	"Last-Modified": function(data) {
		if (data instanceof Date) {
			return Agmd.Date.formatForHeader(data);
		}
		return data;
	},
	"Expires":function(data) {
		if (data instanceof Date) {
			return Agmd.Date.formatForHeader(data);
		}
		return data;
	}
}


HttpResponse.prototype.formatHeaders = function(headers) {
	for (var key in headers) {
		headers[key] = HeadersFormatter[key] && HeadersFormatter[key](headers[key]) || headers[key];
	}
}



HttpResponse.prototype.writeHead = function(statusCode, reasonPhrase, headers) {
	var me = this,
		_headers = reasonPhrase || headers ||  {};
	if (me.encoding)
		_headers['Content-Encoding'] = me.encoding;
	me.formatHeaders(_headers);
	me.resp.writeHead(statusCode, reasonPhrase, headers);

	me.statusCode = statusCode;
	me._headerSent = true;
};



HttpResponse.prototype.write = function(data, encoding) {
	if (!this._headerSent) {
		this.writeHead(200, {});
	}
	this.writer.write(data, encoding);
};



HttpResponse.prototype.end = function(data, encoding) {
	var me = this;
	if (data)
		me.write(data, encoding);

	me.once('finish', function() {
		me.emit('__responseEnd');
	});
	me.writer.end();
	Logger.log('HTTP','RESPONSE',me.request.method,me.request.url,me.resp.statusCode);
};


var functionsNames = [
	"setTimeout", "setHeader", "getHeader",
	"removeHeader", "addTrailers", "on",
	"removeAllListeners", "writeContinue",
	"once", "emit", "removeListener"
];

var wrapFunction = function(fname, proto) {
	proto[fname] = function() {
		var tmp;
		return (tmp = this.resp[fname].apply(this.resp, arguments)) && tmp == this.resp ? this : tmp;
	};
};
for (var i in functionsNames)
	wrapFunction(functionsNames[i], HttpResponse.prototype);

module.exports = HttpResponse;