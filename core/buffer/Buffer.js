Agmd.define({
	_position: 0,
	_bufferSize: 0,

	ctor: function(arg1) {

		var me = this,
			baseBuffer = typeof arg1 != "number" && arg1 || '';
		me._bufferSize = typeof arg1 == "number" && Agmd.Number.getNearestPow2(arg1) || baseBuffer.length && Agmd.Number.getNearestPow2(baseBuffer.length) || 256;

		me._buffer = new Buffer(me._bufferSize);
		if (baseBuffer && baseBuffer.length) {
			me.write(baseBuffer);
		}
	},
	write: function(data) {

		var me = this;
		if (me._bufferSize < (me._position + data.length)) {
			me._resizeBuffer(Agmd.Number.getNearestPow2(me._position + data.length));
		}
		me._buffer.write(data, me._position, data.length, 'utf8');
		me._position += data.length;
	},
	_resizeBuffer: function(newSize) {
		var me = this;
		if (newSize <= me._bufferSize) {
			return;
		}

		var buffer = new Buffer(newSize);
		me._buffer.copy(buffer);
		me._buffer = buffer;
		me._bufferSize = newSize;
	},
	getData: function() {
		return this._buffer;
	},
	toString: function() {
		return this._buffer.toString('utf8',0,this._position);
	}
});