var protocol = {
        'http:': require('http'),
        'https:': require('https')
    },
    url = require('url');

Agmd = Agmd || {};
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
Agmd.Request = {
    redirectCode: {
        301: 1,
        302: 1
    },
    get: function(_url, _opt, callback) {
        var opt = callback ? _opt : {};
        callback = callback || _opt;

        opt.method = "GET";

        this.request(_url, opt, callback);
    },
    post: function(_url, _opt, callback) {
        var opt = callback ? _opt : {};
        callback = callback || _opt;

        opt.method = "POST";
        this.request(_url, opt, callback);
    },
    request: function(_url, _opt, callback) {
        var me = this;
        var opt = callback ? _opt : {};
        callback = callback || _opt;
        opt.method = opt.method || "GET";

        var parsed = url.parse(_url);
        opt.port = parsed.port || opt.port || '80';
        opt.hostname = parsed.hostname || opt.hostname;
        opt.path = parsed.path;
        var _protocol = parsed.protocol || opt.protocol || 'http:';
        Logger.log('HTTPCLIENT', 'REQUEST', opt.method, _protocol + '//' + opt.hostname + ':' + opt.port, opt.path);
        var req = protocol[_protocol].request(opt, function(res) {

            Logger.log('HTTPCLIENT', 'RESPONSE', opt.method, _protocol + '//' + opt.hostname + ':' + opt.port, opt.path, res.statusCode);
            if (Agmd.Request.redirectCode[res.statusCode]) {
                var location = res.headers.location;
                if (location) {
                    Logger.debug('redirect location', location);
                    opt.protocol = parsed.protocol || opt.protocol;
                    me.request(location, opt, callback);
                } else {
                    callback('redirect but no location (redirect nowhere)');
                }
                return;
            }

            callback(null, req, res);
        });
        req.on('error', function(e) {
            Logger.error(e);
            callback(e);
        });

        if (opt.__data)
            req.write(opt.__data);
        req.end();
    }
};