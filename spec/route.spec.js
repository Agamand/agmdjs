require('../index.js');
var url = Core.Route.url,
	action_url = Core.Route.action_url,
	action = Core.Route.action,
	res = Core.Route.resource;



var route = {
	'//a': url('out1'),
	'//a//b': url('out2'),
	'//a//b//c': url('out3'),
	'//a//b//%(truc|machin)': url('out4'),
	'//a//b//%(truc|machin)//d': url("out5"),
	'//a//b//%(truc|machin)//c': url("out6"),
	'//a//b//%(truc|machin)//%(bidule|kel)': url("out7"),
	'//a//b//%(truc|machin)//%(bidule|kel)//add': url("out8"),
	'//a//d//%(.*)': url("out9")

}

var queries = [
	['/a', 'out1'],
	['/a/b', 'out2'],
	 ['/a/b/truc', 'out4'],
	 ['/a/b/machin', 'out4'],
	 ['/a/b/bidule', false],
	  ['/a/b/machin/d', 'out5'],
	  ['/a/b/machin/c', 'out6'],
	  ['/a/b/truc/bidule', 'out7'],
	  ['/a/b/truc/bidule/add', 'out8'],
	 ['/a/d/m/d/f/s', 'out9']
]



Core.RoutingTree = Core.Route.createRouteTree(route);


describe('Route unit test', function() {
	for (var i in queries) {
		(function(query) {
			it(['test route with path ', query[0]].join(''), function(done) {
				var result = Core.Route.route(query[0]);
				if (!result) {
					Logger.debug(query[0], result, query[1]);
					expect(result).toBe(query[1]);
				} else if (result && result.path) {
					Logger.debug(query[0], result, query[1]);
					expect(result.path).toBe(query[1]);
				} else {
					Logger.debug(query[0], result, query[1]);
				}
				done();
			});
		})(queries[i]);
	}
});