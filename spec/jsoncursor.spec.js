require('../index.js');
Core.include('AgmdJS.database.JSONDatabase');
var JSONCursor = Core.include('AgmdJS.database.JSONCursor'),
	async = Core.Async;
describe('JSONCursor unit test', function() {
	it('JSONCursor.next', function(done) {
		var samples = [1, 2, 3, 4, 5];
		var cursor = new JSONCursor(samples);
		var tasks = [];
		for (var i = 0, l = samples.length; i <= l; i++) {
			tasks.push((function(value) {
				return function(cb) {
					cursor.next(function(err, data) {
						expect(data).toBe(value);
						cb();
					})
				}
			})(samples[i] || null))
		}
		async.series(tasks, function() {
			done();
		});

	})

	it('JSONCursor.forEach', function(done) {
		var samples = [1, 2, 3, 4, 5];
		var cursor = new JSONCursor(samples);
		var itr = 0;
		async.series([function(cb) {
			cursor.forEach(function(err, data) {
				expect(data).toBe(samples[itr++] || null);
				if (null === data)
					cb();
			})
		}], function() {
			done();
		});

	})

	it('JSONCursor.toArray', function(done) {

		var samples = [1, 2, 3, 4, 5];

		var cursor = new JSONCursor(samples);
		cursor.toArray(function(err, data) {
			expect(data).toEqual(samples);
			done();
		});
	})



	it('test the cursor asynchronism', function(done) {

		var expectedOrder = ['r', 'p', 'r', 'p', 'r', 'p', 'r', 'p', 'r', 'p', 'r'];
		var order = [];

		var samples = [1, 2, 3, 4, 5];
		var cursor = new JSONCursor();
		var itr = 0;

		cursor.push(samples[itr++]);

		cursor.forEach(function(err, data) {
			order.push('r');
			if (null === data) {
				expect(order).toEqual(expectedOrder);
				done();
			}
		});

		var tasks = [];
		for (; itr <= samples.length; itr++) {
			tasks.push((function(data) {
				return function(cb) {
					setImmediate(function() {
						order.push('p');
						cursor.push(data || null);
						cb();	
					})
				};
			})(samples[itr]));
		}
		async.series(tasks,function(){
			console.log('done')
		});
	})
})