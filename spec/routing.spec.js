require('../index.js');
var url = Core.Routing.url,
    action_url = Core.Routing.action_url,
    action = Core.Routing.action,
    res = Core.Routing.resource;



var route = {
    '/a': url('out1'),
    '/a/b': url('out2'),
    '/a/b/c': url('out3'),
    '/a/b/$p1': url('out4'), //(truc|machin)
    '/a/b/$p1/d': url("out5"),
    '/a/b/$p1/c': url("out6"),
    '/a/b/$p1/$p2': url("out7"), //(bidule|kel)
    '/a/b/$p1/$p2/add': url("out8"),
    '/a/d/$p1': url("out9") //(.*)

}

var queries = [
    ['/a', 'out1'],
    ['/a/b', 'out2'],
    ['/a/b/truc', 'out4'],
    ['/a/b/machin', 'out4'],
    ['/a/b/bidule', false],
    ['/a/b/machin/d', 'out5'],
    ['/a/b/machin/c', 'out6'],
    ['/a/b/truc/bidule', 'out7'],
    ['/a/b/truc/bidule/add', 'out8'],
    ['/a/d/m/d/f/s', 'out9']
]



Core.RoutingTree = Core.Routing.createRouteTree(route);


describe('Routing unit test', function() {
    for (var i in queries) {
        (function(query) {
            it(['test route with path ', query[0]].join(''), function(done) {
                var result = Core.Routing.route(query[0]);
                if (!result) {
                    Logger.debug(query[0], result, query[1]);
                    expect(result).toBe(query[1]);
                } else if (result && result.path) {
                    Logger.debug(query[0], result, query[1]);
                    expect(result.path).toBe(query[1]);
                } else {
                    Logger.debug(query[0], result, query[1]);
                }
                done();
            });
        })(queries[i]);
    }
});