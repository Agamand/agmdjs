return;
require('../index.js');
var dummies = [{
	id: 1,
	name: 'dummy1',
	data: {
		title: "t1",
		desc: 'desc1'
	}
}, {
	id: 2,
	name: 'dummy2',
	data: {
		title: "t2",
		desc: 'desc2'
	}
}, {
	id: 3,
	name: 'dummy3',
	data: {
		title: "t3",
		desc: 'desc3'
	}
}];



var proj = {
	id: 1,
	'data.title': 1
}

var cproj = Core.Projection.convert(proj);

var proj2 = {
	id: 0
}

var cproj2 = Core.Projection.convert(proj2);


for (var i in dummies) {
	var d = dummies[i];

	console.log('t',Core.traverse(d,'data.title'))

	console.log(d, Core.Projection.project(cproj, d));
}


for (var i in dummies) {
	var d = dummies[i];
	console.log(d, Core.Projection.project(cproj2, d));
}