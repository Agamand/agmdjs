require('../index.js')
var async = Core.Async;

var dummiesData = [{
    id: 1,
    name: 'dummy1',
    data: {
        title: "t1",
        desc: 'desc1'
    }
}, {
    id: 2,
    name: 'dummy2',
    data: {
        title: "t2",
        desc: 'desc2'
    }
}, {
    id: 3,
    name: 'dummy3',
    data: {
        title: "t3",
        desc: 'desc3'
    }
}];

var mongoService = Core.Service.getInstance('AgmdJS.database.MongoDBConnector');
mongoService.insert('test', 'test.collection', dummiesData, function(err, result) {
    expect(err).toBe(null);
    expect(result.insertedCount).toBe(3);
    done();
})






describe('MongoDBConnector Tests', function() {
    var mongoService = Core.Service.getInstance('AgmdJS.database.MongoDBConnector');

    it('MongoDBConnector.dropDatabase callback', function(done) {
        mongoService.dropDatabase('test', function(err, result) {
            done();
        })
    })


    it('MongoDBConnector.insert callback', function(done) {
        mongoService.insert('test', 'test.collection', dummiesData, function(err, result) {
            expect(err).toBe(null);
            expect(result.insertedCount).toBe(3);
            done();
        })

    })


    it('MongoDBConnector.findOne callback', function(done) {
        mongoService.findOne('test', 'test.collection', {
            filter: {
                id: 2
            }
        }, function(err, data) {
            expect(err).toBe(null);
            expect(data.id).toBe(2);
            expect(data.data.title).toBe("t2");
            done();
        })
    })

    it('MongoDBConnector.find callback', function(done) {
        mongoService.find('test', 'test.collection', {
            sort: {
                id: 1
            }
        }, function(err, cursor) {

            expect(err).toBe(null);
            cursor.toArray(function(err, array) {
                expect(err).toBe(null);
                expect(array).toEqual(dummiesData);
                done();
            });
        })

    })


    it('MongoDBConnector.dropDatabase promise', function(done) {
        mongoService.dropDatabase('test').then(function() {
            done();
        }, function() {
            done();
        })
    })


    it('MongoDBConnector.insert promise', function(done) {
        mongoService.insert('test', 'test.collection', dummiesData).then(function(result) {
            expect(result.insertedCount).toBe(3);
            done();
        }, function(err) {
            expect(err).toBe(null);
            done();
        })

    })


    it('MongoDBConnector.findOne promise', function(done) {
        mongoService.findOne('test', 'test.collection', {
            filter: {
                id: 2
            }
        }).then(function(data) {
            expect(data.id).toBe(2);
            expect(data.data.title).toBe("t2");
            done();
        }, function(err) {
            expect(err).toBe(null);
            done();
        })
    })

    it('MongoDBConnector.find promise', function(done) {
        mongoService.find('test', 'test.collection', {
            sort: {
                id: 1
            }
        }).then(function(cursor) {
            cursor.toArray().then(function(array) {
                expect(array).toEqual(dummiesData);
                done();
            }, function(err) {
                expect(err).toBe(null);
                done();
            });
        }, function(err) {
            expect(err).toBe(null);
            done();
        });


    })


})