require('../index.js');

var testZip = __dirname + '/test_data/text.zip';
var testZipPath = "text.txt";
var expectedValue = "1234";

const Archive = Agmd.Archive;
describe('Archive zip test', function() {
	it('Archive.Zip.open test', function(done) {
		try {
			var zip = Archive.getArchive(testZip);
			zip.open();
			var data = zip.readEntryAsText(testZipPath);

			var entries = zip.getEntries();
			console.log("data", data);
			expect(entries).toEqual([testZipPath]);
			expect(data).toEqual(expectedValue);
			done();
		} catch (e) {
			console.log(e);
			done(false);
		}
	})
});