require('../index.js');

describe('Class unit test', function() {
	it('Class.define test simple class', function(done) {

		var Simple = Core.define(null, {
			toto: 1,
			ctor: function() {}
		});
		var inst = new Simple(),
			inst2 = new Simple();
		inst.toto = 2;

		expect(inst.toto).toBe(2);
		expect(inst2.toto).toBe(1);

		done();
	});
});