require('../index.js');
var TestClass = Core.define('AgmdJS.TestClass', {}); // an empty class

require('../index.js');

describe('Event unit test', function() {
	it('Event.on', function(done) {
		var a = new TestClass();
		var value = 0;
		a.on('test', function() {
			++value;
		})
		a.fireEvent('test');
		a.fireEvent('test');
		expect(value).toBe(2);
		done();
	})

	it('Event.once', function(done) {
		var a = new TestClass();
		var value = 0;
		a.once('test', function() {
			++value;
		})
		a.fireEvent('test');
		a.fireEvent('test');
		expect(value).toBe(1);
		done();
	})

})