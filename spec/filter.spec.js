require('../index.js');
var dummies = [{
	id: 1,
	name: 'dummy1',
	data: {
		title: "t1",
		desc: 'desc1'
	},
	rnd: [3, 8, 9]
}, {
	id: 2,
	name: 'dummy2',
	data: {
		title: "t2",
		desc: 'desc2'
	},
	parent: [{
		id: 1
	}],
	rnd: [4, 7, 9]
}, {
	id: 3,
	name: 'dummy3',
	data: {
		title: "t3",
		desc: 'desc3'
	},
	parent: [{
		id: 1
	}],
	rnd: [10, 15, 7]
}, {
	id: 4,
	name: 'dummy4',
	data: {
		title: "t4",
		desc: 'desc4'
	},
	parent: [{
		id: 2
	}],
	rnd: [50, 30, 18]
}];

describe('Filter unit test', function() {
	it('Filter.convert test 1', function(done) {
		var filter = {

			id: {
				$in: [1, 3]
			}

		}
		var cfilter = Core.Filter.convert(filter);
		expect(cfilter).toEqual({
			'id.$in': [1, 3]
		})
		done();
	})

	it('Filter.convert test 2', function(done) {
		var filter = {

			id: 1,
			name: 'dummy1',
			data: {
				title: 'title1'
			}
		}
		var cfilter = Core.Filter.convert(filter);
		expect(cfilter).toEqual({
			'id': 1,
			'name': 'dummy1',
			'data.title': 'title1'
		})
		done();
	})


	it('Filter.has test (simple)', function(done) {
		var filter = {
			id: 1,
			data: {
				title: 't1'
			}
		}
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[0])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[1])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[2])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[3])).toBe(false);
		done();
	})
	it('Filter.has test 2 (simple)', function(done) {
		var filter = {
			id: 1,
			data: {
				title: 't2'
			}
		}
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[0])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[1])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[2])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[3])).toBe(false);
		done();
	})

	it('Filter.has test 2 (simple with test in array)', function(done) {
		var filter = {
			'parent': {
				id: 1
			}
		}
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[0])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[1])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[2])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[3])).toBe(false);
		done();
	})

	it('Filter.has test ($or operator)', function(done) {
		var filter = {

			$or: [{
				id: 1
			}, {
				data: {
					title: 't2'
				}
			}]
		}
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[0])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[1])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[2])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[3])).toBe(false);
		done();
	})

	it('Filter.has test ($in operator)', function(done) {
		var filter = {

			id: {
				$in: [1, 3]
			}
		}
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[0])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[1])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[2])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[3])).toBe(false);
		done();
	})
	it('Filter.has test 2 ($in operator with array as target)', function(done) {
		var filter = {

			rnd: {
				$in: [658, 982, 7]
			}
		}
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[0])).toBe(false);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[1])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[2])).toBe(true);
		expect(Core.Filter.has(Core.Filter.convert(filter), dummies[3])).toBe(false);
		done();
	})


})