require('../index.js');


var dummies = [{
	id: 0,
	name: 'dummy2',
	data: {
		title: "t2",
		desc: 'desc1'
	},
	parent: [{
		id: 2
	}]
}, {
	id: 2,
	name: 'dummy0',
	data: {
		title: "t1",
		desc: 'desc2'
	},
	rnd: [3, 8, 9],
	parent: [{
		id: 1
	}]
}, {
	id: 1,
	name: 'dummy0',
	data: {
		title: "t3",
		desc: 'desc3'
	},
	parent: [{
		id: 2
	}]
}];

describe('Array.sort unit test', function() {
	it('Array.sort', function() {
		var res = Core.Array.sort(dummies, {
			'name': 1,
			'id': 1
		});

		var expectedResult = [1, 2, 0];
		var result = [];
		for (var i in expectedResult)
			result.push(res[i].id)
		expect(result).toEqual(expectedResult);

	});
	it('Array.sort 2', function() {
		var res = Core.Array.sort(dummies, {
			id: -1,
			'parent.id': 1
		});

		var expectedResult = [2, 1, 0];
		var result = [];
		for (var i in expectedResult)
			result.push(res[i].id)
		expect(result).toEqual(expectedResult);

	});
});