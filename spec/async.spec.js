require('../index.js');

describe('Async unit test', function() {
	it('Async.series test', function(done) {

		var result = [];
		Core.Async.series([function(cb) {

			setTimeout(function() {
				result.push(1);
				cb();
			}, 100);
		}, function(cb) {
			setTimeout(function() {
				result.push(2);
				cb();
			}, 100);
		}, function(cb) {
			setTimeout(function() {
				result.push(3);
				cb();
			}, 100);
		}, function(cb) {
			setTimeout(function() {
				result.push(4);
				cb();
			}, 100);
		}], function() {
			expect(result).toEqual([1, 2, 3, 4]);
			done();
		});
	})


	it('Async.parallel test 1', function(done) {
		var result = [];
		Core.Async.parallel([function(cb) {
			setTimeout(function() {
				result.push(1);
				cb();
			}, 300);
		}, function(cb) {
			setTimeout(function() {
				result.push(2);
				cb();
			}, 700);
		}, function(cb) {
			setTimeout(function() {
				result.push(3);
				cb();
			}, 100);
		}, function(cb) {
			setTimeout(function() {
				result.push(4);
				cb();
			}, 500);
		}], function() {
			expect(result).toEqual([3, 1, 4, 2]);
			done();
		});
	})


})