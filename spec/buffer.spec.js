require('../index.js');

var AGBuffer = Core.include('AgmdJS.core.buffer.Buffer');




describe('String Buffer unit test', function() {
	it('Buffer ctor test', function(done) {

		var buffer = new AGBuffer('123');
		expect(buffer._bufferSize).toBe(4);
		expect(buffer.toString()).toBe('123');

		buffer = new AGBuffer(28);
		buffer.write('321');
		expect(buffer._bufferSize).toBe(32);
		expect(buffer.toString()).toBe('321');
		done();
	});


	it('Buffer auto resize test', function(done) {

		var buffer = new AGBuffer('123');
		expect(buffer._bufferSize).toBe(4);
		buffer.write('456');
		expect(buffer._bufferSize).toBe(8);
		expect(buffer.toString()).toBe('123456');
		done();
	});

})