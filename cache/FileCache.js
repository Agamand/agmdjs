var path = require('path'),
	fs = require('fs'),
	async = Core.Async;
Core.define({
	singleton: true,

	cacheStatus: {
		GOOD: 0,
		OUTDATED: 1,
		NOCACHE: 2
	},

	ctor: function() {
		var me = this;
		me.cacheDIR = Core.getConfig('cache.cacheDIR');
		me.defaultCacheDuration = Core.getConfig('cache.defaultCacheDuration') || 3600;
	},

	getCache: function(name, callback) {
		var me = this;

		if (!me.cacheDIR) {
			callback('cacheDIR is not defined');
			return;
		}
		var _pathInfo = path.normalize([me.cacheDIR, '/', name, '.cache.json'].join('')),
			cache = {},
			onEnd = function(err) {
				if (err) {
					cache.cacheStatus = me.cacheStatus.NOCACHE;
				} else {
					var currentDate = new Date();

					cache.createdDate = new Date(cache.createdDate);

					var diff = (currentDate - cache.createdDate);
					if (diff >= (cache.duration * 1000)) {
						cache.cacheStatus = me.cacheStatus.OUTDATED;

					} else cache.cacheStatus = me.cacheStatus.GOOD;
					Logger.debug('GET cache', !!cache.data, cache.cacheStatus, currentDate, cache.createdDate, diff, cache.duration * 1000 - diff, diff >= (cache.duration * 1000));
				}
				callback(null, cache);
			};

		async.series([function(cb) {
			fs.exists(_pathInfo, function(exists) {
				cb(exists ? null : 'NOCACHE');
			});
		}, function(cb) {
			try {
				var data = require(_pathInfo);

				cache.duration = data.duration;
				cache.createdDate = data.createdDate;
				cache.filename = data.filename;
				fs.readFile(path.normalize([me.cacheDIR, '/', cache.filename].join('')), function(err, buffer) {
					Logger.debug([me.cacheDIR, '/', cache.filename].join(''),arguments);
					cache.data = buffer;
					cb(err);
				});

			} catch (e) {
				cb(e);
				return;
			}


		}], onEnd);
	},
	saveCache: function(name, data, opt, callback) {
		var me = this;
		if (!me.cacheDIR) {
			callback('cacheDIR is not defined');
			return;
		}
		var _path = path.normalize([me.cacheDIR, '/', name, '.cache'].join('')),
			_pathInfo = path.normalize([me.cacheDIR, '/', name, '.cache.json'].join('')),
			cache = {
				filename: [name, '.cache'].join(''),
				duration: opt && opt.duration !== undefined && opt.duration || me.defaultCacheDuration,
				createdDate: new Date()
			};
		Core.Fs.makePathSync(_path);
		async.series([function(cb) {
			fs.writeFile(_pathInfo, JSON.stringify(cache), cb);
		}, function(cb) {
			fs.writeFile(_path, data, cb);
		}], function(err) {
			callback(err,cache);
		});
	}

});
