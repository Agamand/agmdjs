var FileCache = Core.include('AgmdJS.cache.FileCache');
Core.define({
	singleton: true,
	cacheStatus: FileCache.cacheStatus,
	getCache: function(name, callback) {
		var me = this;
		if (me.memoryCache && me.memoryCache[name]) {
			var cache = me.memoryCache[name];
			var currentDate = new Date();
			cache.createdDate = new Date(cache.createdDate);
			var diff = (currentDate - cache.createdDate);
			if (diff >= (cache.duration * 1000)) {
				cache.cacheStatus = FileCache.OUTDATED;

			} else cache.cacheStatus = FileCache.GOOD;
			callback(null, cache);
			return;
		}
		FileCache.getCache(name, function(err, cache) {
			if (!err && cache.data) {

				cache.data = JSON.parse(cache.data.toString());
				me.memoryCache = me.memoryCache || {};
				me.memoryCache[name] = cache;
			}
			callback(err, cache);
		});
	},
	saveCache: function(name, data, opt, callback) {
		var me = this;
		FileCache.saveCache(name, JSON.stringify(data), opt, function(err, cacheResult) {
			if (!err) {
				me.memoryCache = me.memoryCache || {};
				me.memoryCache[name] = cacheResult;
				me.memoryCache[name].data = data;
			}
			callback(err, cacheResult);
		});
	}
});