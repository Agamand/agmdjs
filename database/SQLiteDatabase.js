var sqlite3 = require('sqlite3').verbose();

Core.define({
	extendOf: "AgmdJS.service.Service",
	DBPath: null,
	ctor: function(file) {
		var me = this;
		if (file) {
			me.DBPath = file;
			me.loadFile();
		}
	},
	loadFile: function() {
		var me = this;
		if (!me.DBPath)
			me.data = new sqlite3.Database(':memory:');
		else me.data = new sqlite3.Database(me.DBPath);
	},
	sql: function(sql, callback) {
		var me = this;
		me.data.each(sql, function(err, row) {
			callback(err, row);
		}, function() {
			callback();
		});
	},

	filterToSQL: function(filter) {

		var me = this,
			res = [];
		/*for (var key in filter) {

			var v, f = filter[key];

			if(key == '$or'){
				var inter = [];
				for(var j in f){
					var r = me.filterToSQL()
			}

			if (f instanceof Array)
				v = ['(', f.join(', '), ')'].join('');
			else if (f == 'object') {
			{
				if(f.$in){

				}else{

				}
				v = me.filterToSQL(v);
			} else v = ' = ' + f;
			res
		}*/
		return res.join(' AND ');
	},
	find: function(table, filter, projection) {
		projection = projection || {};
		filter = filter || {};


		var p, q;

		return null;
	},
	findOne: function(filter, projection, callback) {
		var me = this;
		callback = callback || projection || filter;
		projection = projection != callback && projection || {};
		filter = filter != callback && filter || {};
		if (!callback)
			return;
		callback(null, null);

	}
});