/*

NEED TO BE REFACTORED

*/

var fs = require('fs'),
    path = require('path'),
    crypto = require('crypto'),
    MongoClient = require('mongodb').MongoClient,
    ObjectID = require('mongodb').ObjectID,
    async = Core.Async,
    Deferred = Core.Deferred;
Core.define({
    extendOf: "AgmdJS.database.GenericConnector",
    waitCall: [],
    ObjectID: ObjectID,
    ObjectIDFromInt: function(value) {
        value = ("" + value).toString(16);
        for (var i = 0, l = 24 - value.length; i < l; ++i)
            value = 0 + value;
        return new this.ObjectID(value);
    },
    ctor: function(cfg) {
        var me = this,
            url = ['mongodb://'];

        cfg = cfg || Core.getConfig('database.mongodb') || {
            host: '127.0.0.1'
        };
        if (cfg.login && cfg.password) {
            url.push(cfg.login, ':', cfg.password, '@');
        }
        url.push(cfg.host);
        if (cfg.port) {
            url.push(cfg.port);
        }
        url = url.join('');
        MongoClient.connect(url, function(err, db) {
            if (err) {
                me.error(err);
                return;
            }
            me.client = db;
            me.init = true;
            me._processWaitCall();
        });

    },
    _pushToWaitCall: function(handler, args) {
        var me = this,
            deferred = Core.Deferred();
        me.waitCall.push({
            handler: handler,
            args: args,
            deferred: deferred
        });
        return deferred.promise();
    },
    _processWaitCall: function() {
        var me = this;

        for (var i in me.waitCall) {
            var call = me.waitCall[i];
            call.handler.apply(me, call.args).then(function(data) {
                call.deferred.resolve(data);
            }, function(err) {
                call.deferred.reject(err);
            });
        }
        me.waitCall = [];
    },

    _getCollectionFromDB: function(dbName, collectionName, callback) {
        var me = this,
            db = me.client.db(dbName);
        if (callback)
            callback(null, db.collection(collectionName));
        return db.collection(collectionName);
    },
    /* M1 args : (dbName, collection, callback)*/
    /* M2 args : (dbName, collection, opts, callback)*/
    find: function(dbName, collectionName, opts, callback) {
        var me = this;
        if (!me.init) {
            return me._pushToWaitCall(me.find, arguments);
        }
        if (arguments.length < 4 && typeof opts == 'function') {
            callback = opts;
            opts = {};
        }
        return new Promise(function(resolve, reject) {
            var collection = me._getCollectionFromDB(dbName, collectionName);
            var cursor = collection.find().addCursorFlag('awaitData', false);
            for (var k in opts) {
                cursor = cursor[k](opts[k]);
            }
            //workaround
            cursor = cursor
            if (callback)
                callback(null, cursor);
            resolve(cursor);
        });
    },
    /* M1 args : (dbName, collection, callback)*/
    /* M2 args : (dbName, collection,  opts, callback)*/
    findOne: function(dbName, collectionName, opts, callback) {
        var me = this;
        if (!me.init) {
            return me._pushToWaitCall(me.findOne, arguments);
        }
        var collection = me._getCollectionFromDB(dbName, collectionName);
        var cursor = collection.find();
        for (var k in opts) {
            if (typeof cursor[k] == 'function') {
                cursor = cursor[k](opts[k]);
            } else {
                me.error(cursor[k], k, 'is not a function');
            }
        }
        return cursor.limit(1).next(callback);

    },
    insert: function(dbName, collectionName, docs, callback) {
        var me = this;
        if (!me.init) {
            return me._pushToWaitCall(me.insert, arguments);
        }
        var collection = me._getCollectionFromDB(dbName, collectionName);

        if (docs instanceof Array)
            return collection.insertMany(docs, callback);
        else return collection.insertOne(docs, callback);

    },
    bulkInsert: function(dbName, collectionName, docs, callback) {
        var me = this;
        if (!me.init) {
            return me._pushToWaitCall(me.insert, arguments);
        }

        var collection = me._getCollectionFromDB(dbName, collectionName);
        if (!(docs instanceof Array))
            docs = [docs];

        var bulk = collection.initializeUnorderedBulkOp();
        for (var i = 0; i < docs.length; i++) {
            bulk.insert(docs[i]);
        }
        ;
        return bulk.execute(callback);
    },
    updateOne: function(dbName, collectionName, filter, data, callback) {
        var me = this;
        if (!me.init) {
            return me._pushToWaitCall(me.insert, arguments);

        }
        var collection = me._getCollectionFromDB(dbName, collectionName);
        return collection.updateOne(filter, data, callback);


    },
    updateMany: function(dbName, collectionName, filter, data, callback) {
        var me = this;
        if (!me.init) {
            return me._pushToWaitCall(me.insert, arguments);
        }
        var collection = me._getCollectionFromDB(dbName, collectionName);
        return collection.updateMany(filter, data, callback);

    },
    dropDatabase: function(dbName, callback) {
        var me = this;
        if (!me.init) {
            return me._pushToWaitCall(me.dropDatabase, arguments);
        }
        var db = me.client.db(dbName);
        return db.dropDatabase(callback);
    },
    dropCollection: function(dbName, collectionName, callback) {
        var me = this;
        if (!me.init) {
            return me._pushToWaitCall(me.dropDatabase, arguments);

        }
        var db = me.client.db(dbName);
        return db.dropCollection(collectionName, callback);
    }
});