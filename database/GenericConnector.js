Core.define({
	extendOf: "AgmdJS.service.Service",
	ctor: function(cfg) {
		var me = this;
	},
	find: function(dbName, collectionName, filter, projection, callback) {
		var me = this;
	},
	findOne: function(dbName, collectionName, filter, projection, callback) {
		var me = this;
	},
	insert: function(dbName, collectionName, docs, callback) {
		var me = this;
	},
	updateOne: function(dbName, collectionName, filter, data, callback) {
		var me = this;
	},
	updateMany: function(dbName, collectionName, filter, data, callback) {
		var me = this;
	},
	dropDatabase: function(dbName, callback) {
		var me = this;
	},
	dropCollection: function(dbName, collectionName, callback) {
		var me = this;
	}
});