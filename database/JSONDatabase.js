/*

NEED TO BE REFACTORED
*/

var fs = require('fs'),
	path = require('path'),
	crypto = require('crypto');

var JSONCursorProto = {
	data: null,
	position: 0,
	ctor: function(data) {
		var me = this;
		if (data) {
			me.data = Core.copy(data);
			me.end(); //close cursor²
		}
	},
	next: function(callback) {
		var me = this;
		if (!me._end && (!me.data || !me.data[me.position] || me.sorted)) {
			me.wait = me.wait || [];
			me.wait.push({
				p: me.position++,
				c: callback
			});
		} else callback(null, me.data && me.data[me.position++] || null);
		return me;
	},
	push: function(data) {
		var me = this;
		if (!data)
			return me.end();
		if (!(data instanceof Array))
			data = [data];
		me.data = me.data || [];
		for (var i in data)
			me.data.push(data[i]);
		return !me.sorted ? me.__processWaitList() : me;
	},

	rewind: function() {
		var me = this;
		me.position = 0;
		return me;
	},
	sort: function(filter) {
		// apply filter
		var me = this;
		me.sorted = filter;
		if (me.end && me.data)
			me.data = Core.Array.sort(me.data, me.sorted);
		return me;
	},
	end: function() {
		//after that, no data can't be add
		var me = this;
		me._end = true;
		if (me.sorted)
			me.data = Core.Array.sort(me.data, me.sorted);
		me.__processWaitList();
		return me;
	},
	forEach: function(callback) {
		var me = this;
		var next = function(err, data) {
			callback(err, data);
			if (err || !data)
				return;
			process.nextTick(function() {
				me.next(next);
			});
		}

		me.next(next)

	},
	toArray: function(callback) {
		var me = this,
			res = [];

		var i = 0;
		me.forEach(function(err, data) {
			if (err) {
				callback(err);
				return;
			}
			if (!data) {
				callback(err, res);
				return;
			}
			res.push(data);
		})
		return me;
	},
	__processWaitList: function() {
		var me = this;
		if (me.wait && me.wait.length) {
			var stop = null,
				l = me.wait.length;
			for (var i = 0; i < l; ++i) {
				var w = me.wait[i];
				if (me._end) {
					w.c(null, me.data && me.data[w.p] || null);
				} else if (me.data && me.data[w.p]) {
					w.c(null, me.data[w.p]);
				} else {
					stop = +i;
					break;
				}

			}
			if (null == stop)
				stop = l;
			if (stop) {
				if (stop == me.wait.length)
					me.wait = null;
				else me.wait.splice(0, stop);
			}
		}
		return me;
	}
};
JSONCursorProto.foreach = JSONCursorProto.forEach; // retro-compatibility
var JSONCursor = Core.define('AgmdJS.database.JSONCursor', JSONCursorProto);



Core.define({
	extendOf: "AgmdJS.service.Service",
	JSONPath: null,
	ctor: function(file) {
		var me = this;
		if (file) {
			me.JSONPath = file;
			me.loadFile();
		}
	},
	loadFile: function() {
		var me = this;
		if (!me.JSONPath)
			return;
		me.data = require(me.JSONPath)
	},
	/* args : ([filter, projection],callback)*/
	find: function(filter, projection) {
		projection = projection || {};
		filter = filter || {};
		filter = Core.Filter.convert(filter);
		var me = this,
			cur = new JSONCursor();
		for (var i in me.data) {
			if (Core.Filter.has(filter, me.data[i]))
				cur.push(Core.Projection.project(projection, me.data[i]));
		}
		return cur.push(null);
	},
	findOne: function(filter, projection, callback) {
		var me = this;
		callback = callback || projection || filter;
		projection = projection != callback && projection || {};
		filter = filter != callback && filter || {};
		filter = Core.Filter.convert(filter);
		if (!callback)
			return;
		//not implemented
		for (var i in me.data) {
			if (Core.Filter.has(filter, me.data[i])) {
				callback(null, Core.Projection.project(projection, me.data[i]));
				return;
			}
		}
		callback(null, null);

	}
});