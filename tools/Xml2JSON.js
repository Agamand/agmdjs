var expat = require('node-expat')

Core.define({
	singleton: true,
	ctor: function() {

	},
	toJSON: function(xml, callback) {

		var result = [],
			stack = [],
			/*		validTag = {
					-	currentTime: 1,sab
						row: 1
					},*/
			parser = new expat.Parser('UTF-8'),
			tmp;


		parser.on('startElement', function(name, attrs) {

			for (var i in attrs) {
				var v = attrs[i];
				if ((+v) + "" == v)
					attrs[i] = +v;
			}
			stack.push({
				attrs: attrs,
				tag: name
			});
			
		});
		parser.on('text', function(text) {
			if (stack.length) {
				stack[stack.length - 1].text = text;
			}
		});
		parser.on('endElement', function(name) {

			var data = stack.pop();
			if (!stack.length)
				result.push(data);
			else
				(stack[stack.length - 1].children = stack[stack.length - 1].children || []).push(data);

		});
		parser.on('error', function(error) {
			Logger.error(error);
		});
		parser.on('end', function() {
			if (callback) callback(null, result);
		});
		if (typeof xml == 'string') {
			parser.write(xml);
			if (callback) callback(null, result);
		} else xml.pipe(parser);


	},
	toJSON2: function(xml, callback) {

		var result = [],
			stack = [],
			/*		validTag = {
					-	currentTime: 1,sab
						row: 1
					},*/
			parser = new expat.Parser('UTF-8'),
			tmp;


		parser.on('startElement', function(name, attrs) {

			for (var i in attrs) {
				var v = attrs[i];
				if ((+v) + "" == v)
					attrs[i] = +v;
			}
			stack.push({
				_attrs: attrs,
				_tag: name
			});
		});
		parser.on('text', function(text) {
			if (stack.length) {
				stack[stack.length - 1].text = text;
			}
		});
		parser.on('endElement', function(name) {

			var data = stack.pop();
			if (!stack.length)
				result.push(data);
			else {

				//(stack[stack.length - 1].children = stack[stack.length - 1].children || []).push(data);
				(stack[stack.length - 1][data._tag] = stack[stack.length - 1][data._tag] || []).push(data);
			}
		});
		parser.on('error', function(error) {
			Logger.error(error);
		});
		parser.on('end', function() {
			if (callback) callback(null, result);
		});
		if (typeof xml == 'string') {
			parser.write(xml);
			if (callback) callback(null, result);
		} else xml.pipe(parser);


	}
})