var YAML = require('js-yaml');
Core.define({
	singleton: true,
	ctor: function() {

	},
	toJSON: function(yaml, callback) {

		var parse = function(yaml) {
			try {
				var doc = YAML.safeLoad(yaml);
				if (callback)
					callback(null, doc);
			} catch (e) {
				console.log(e);
				if (callback)
					callback(e);
			}
		}
		parse(yaml, callback);

	}
})