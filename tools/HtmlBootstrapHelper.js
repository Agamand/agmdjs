Core.define({
	singleton: true,
	ctor: function() {

	},
	table: function(id, headers, rows) {

		var _table = {
			id: id,
			headers: headers || [],
			rows: rows || [],
			setHeaders: function(h) {
				this.headers = h;
				return table;
			},
			setRows: function(r) {
				this.rows = r;
				return table;
			},
			addRow: function(r) {
				this.rows.push(r);
				return table;
			},
			toHtml: function(buffer) {
				var b = buffer || [];

				buffer.push(
					'<table ', _table.id ? 'id="' + _table.id + '" ' : '', 'class="table table-striped">'
				);
				if (this.headers && this.headers.length) {
					buffer.push('<thead><tr>');
					for (var i in this.headers) {
						var h = this.headers[i];
						buffer.push('<th>', h, '</th>')
					}
					buffer.push('</tr></thead>');
				}
				if (this.rows) {
					buffer.push('<tbody>');
					for (var i in this.rows) {
						var r = this.rows[i];
						buffer.push('<tr>')
						for (var j in r) {
							var v = r[j];
							buffer.push('<td>', v, '</td>')
						}
						buffer.push('</tr>')
					}
					buffer.push('</tbody>');
				}
				buffer.push('</table>');
				if (!buffer)
					return b.join('');
			}
		}
		return _table;
	}
})