Core.define({
	singleton: true,
	elt: function(elt, opt, b) {


		var _opt;
		if (arguments.length > 2)
			_opt = opt || {};
		else _opt = {}, b = opt;
		_opt.elt = elt;
		return new HtmlComponent(_opt, b);
	},
	table: function(opt, b) {
		return new Table(opt, b);
	},
	body: function(opt, b) {
		opt = opt || {};
		opt.elt = 'body';
		return new HtmlComponent(opt, b);
	},
	BootstrapPanel: function(opt, b) {

		return new BootstrapPanel(opt.b)
	}
})


var StringBuffer = Core.include('AgmdJS.core.buffer.Buffer');

var HtmlComponent = Core.define('AgmdJS.tools.HtmlComponent', {
	_allowPrepend: false,
	_closed: false,
	_autoClose: true,
	ctor: function(opt, b) {
		opt = opt || {};
		var me = this,
			parent = b instanceof HtmlComponent && b;
		me._buffer = parent && parent.getBuffer() || b || new StringBuffer();
		me._htmlElt = opt.elt || 'div';

		if (false == opt.autoClose)
			me._autoClose = false;

		me._buffer.write(['<', me._htmlElt, ''].join(''));

		for (var key in opt.attrs) {
			me._buffer.write([' ', key, '="', opt.attrs[key], '"'].join(''))
		}
		me._buffer.write('>');
	},
	append: function(callback) {
		var me = this;
		callback.call(me);
		return me._autoClose && me.close() || me;
	},
	appendText: function(str) {
		var me = this
		me._buffer.write(str);
		return me._autoClose && me.close() || me;
	},
	close: function() {
		var me = this;
		if (me._closed)
			return me;
		me._buffer.write(['</', me._htmlElt, '>'].join(''));
		return me;
	},
	getBuffer: function() {
		return this._buffer;
	}
});

var Table = Core.define('AgmdJS.tools.Table', {
	extendOf: 'AgmdJS.tools.HtmlComponent',
	ctor: function(opt, b) {
		opt = opt || {};
		opt.elt = 'table';
		this._super(opt, b);
	},
	appendHeaders: function(callback) {
		var me = this;
		me._buffer.write('<thead>');
		callback.call(me);
		me._buffer.write('</thead>');
		return me;
	},
	appendRows: function(callback) {
		var me = this;
		me._buffer.write('<tbody>');
		callback.call(me);
		me._buffer.write('</tbody>');
		return me.close();
	}
})

var BootstrapPanel = Core.define('AgmdJS.tools.BootstrapPanel', {
	extendOf: 'AgmdJS.tools.HtmlComponent',
	ctor: function(opt, b) {
		var me = this;
		opt = opt || {};
		opt.elt = 'div';
		opt.attrs = opt.attrs || {}
		opt.attrs['class'] = 'panel panel-default ' + (opt.attrs['class'] || '');

		this._super(opt, b);
		if (opt.title) {
			me._buffer.write(['<div class="panel-heading">',
				'<h3 class="panel-title">', opt.title, '</h3>',
				'</div>'
			]);
		}
		me._buffer.write('<div class="panel-body">');
	},
	close: function() {
		var me = this;
		me._buffer.write('</div>');
		return this._super();
	}
})